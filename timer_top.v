`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:48:29 11/03/2013 
// Design Name: 
// Module Name:    timer_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module timer_top(
		clk,
		reset,
		TMOD_in,
		TCON_in,
		TCON_5_out,
		TCON_7_out,
		pin_T0,
		pin_T1,
		pin_INT0,
		pin_INT1,
		TMR_in,
		TMR0_out,
		TMR1_out,
		TMR0_H_wen,
		TMR0_L_wen,
		TMR1_H_wen,
		TMR1_L_wen
    );

	
	
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES
     * Eacho I/O signal must be defined on a seperate line
     * All I/Os other than the clk, reset and standard interfaces (ex. local-link, axi4) should contain
     * an '_in' or '_out' suffix
     * Comment your I/O by block and by signal
     */
    input                               clk;
    input                               reset;
	output								TCON_5_out;
	output								TCON_7_out;
    input 		[7:0]							 TMOD_in;
	input		[7:0]							 TCON_in;	
	 
	 //external clock sources
	 input										pin_T0;  // port3,pin4 for timer0
	 input										pin_T1;  // port3,pin5 for timer1
	 
	 //external INT enable
	 input										pin_INT0;  // port3,pin2 for timer0
	 input										pin_INT1;  // port3,pin3 for timer1
	 
	 //16 bit Timer registers
	 input 			[7:0]						TMR_in;
	 output			[15:0]						TMR0_out;
	 output			[15:0]						TMR1_out;
	 
	 //external timer register write enables
	 input										TMR0_H_wen;
	 input										TMR0_L_wen;
	 input										TMR1_H_wen;
	 input										TMR1_L_wen;
	 
	
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------

	
//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------


	
timer0 timer0_mod1 (
    .clk(clk), 
    .reset(reset), 
    .TMOD_in(TMOD_in[2:0]), 
    .TCON_strt_in(TCON_in[4]), 
    .TMR0_overflow(TCON_5_out), 
    .pin_T0(pin_T0), 
    .pin_INT0(pin_INT0), 
    .TMR0_in(TMR_in), 
    .TMR0_H_wen(TMR0_H_wen), 
    .TMR0_L_wen(TMR0_L_wen), 
    .TMR0_out(TMR0_out)
    );
	
timer0 timer1_mod1 (
    .clk(clk), 
    .reset(reset), 
    .TMOD_in(TMOD_in[6:4]), 
    .TCON_strt_in(TCON_in[6]), 
    .TMR0_overflow(TCON_7_out), 
    .pin_T0(pin_T1), 
    .pin_INT0(pin_INT1), 
    .TMR0_in(TMR_in), 
    .TMR0_H_wen(TMR1_H_wen), 
    .TMR0_L_wen(TMR1_L_wen), 
    .TMR0_out(TMR1_out)
    );
	

endmodule
