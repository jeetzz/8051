`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:25:22 10/14/2013 
// Design Name: 
// Module Name:    ins_buf 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ins_buf(
        clk,
        reset,
    
        ins_2_in,
        ins_1_in,
        ins_0_in,
        
        op_out,
        op1_out,
        op2_out,
        
        control_branchz_in,
        cz_flag_in,
        shifter_in,
        p_mem_in
    );
    
//---------------------------------------------------------
// lOCALPARAMS
//---------------------------------------------------------

    localparam  INSTRUCTION_WIDTH = 8;
    
//---------------------------------------------------------
//I/O
//---------------------------------------------------------

    input                                   clk;
    input                                   reset;
    
    input   [INSTRUCTION_WIDTH - 1:0]       ins_0_in;
    input   [INSTRUCTION_WIDTH - 1:0]       ins_1_in;
    input   [INSTRUCTION_WIDTH - 1:0]       ins_2_in;
    
    output   [INSTRUCTION_WIDTH - 1:0]      op_out;
    output   [INSTRUCTION_WIDTH - 1:0]      op1_out;
    output  [INSTRUCTION_WIDTH - 1:0]       op2_out;
    
    input                                   control_branchz_in;
    input                                   cz_flag_in;
    input   [1:0]                           shifter_in;
    input                                   p_mem_in;
    
//---------------------------------------------------------
//Internal Regs and wires
//---------------------------------------------------------

    reg    [INSTRUCTION_WIDTH - 1:0]        op2;
    reg    [INSTRUCTION_WIDTH - 1:0]        op1;
    reg    [INSTRUCTION_WIDTH - 1:0]        op;
                                            
    reg    [INSTRUCTION_WIDTH - 1:0]        temp_ir6;
    reg    [INSTRUCTION_WIDTH - 1:0]        temp_ir5;
    reg    [INSTRUCTION_WIDTH - 1:0]        temp_ir4;
    reg    [INSTRUCTION_WIDTH - 1:0]        temp_ir1;
    reg    [INSTRUCTION_WIDTH - 1:0]        temp_ir2;
    reg    [INSTRUCTION_WIDTH - 1:0]        temp_ir3;
    
    reg                                     not_branched;



    
//---------------------------------------------------------
//Implementation
//---------------------------------------------------------

    assign op_out = op;
    assign op1_out = op1;
    assign op2_out = op2;
    
    
    always@(posedge clk) begin
    
        if(reset == 1) begin
            op1 <= 8'h00;
            op2 <= 8'h00;
            not_branched <= 1'b0;
        end
        else begin
            if(p_mem_in == 1'b0) begin
                temp_ir1 <= op;
                temp_ir2 <= op1;
                temp_ir3 <= op2;
                temp_ir4 <= ins_0_in;
                temp_ir5 <= ins_1_in;
                temp_ir6 <= ins_2_in;
            end
            else if(control_branchz_in == 1'b1) begin
                if(cz_flag_in == 1'b0) begin
                    op  <= temp_ir1;
                    op1 <= temp_ir2;
                    op2 <= temp_ir3;
                    not_branched <= 1'b1;
                end
                else begin
                    op  <= ins_0_in;
                    op1 <= ins_1_in;
                    op2 <= ins_2_in;
                end
            end
            else begin
                case(shifter_in) 
                    2'd0 : begin
                        op  <= op;
                        op1 <= op1;
                        op2 <= op2;
                        not_branched <= 1'b0;
                        // ir4 <= ir4;
                        // ir5 <= ir5;
                        // ir6 <= ir6;
                    end
                    2'd1 : begin
                        if(not_branched == 1'b1) begin
                            op  <= op1;
                            op1 <= op2;
                            op2 <= temp_ir4;
                            not_branched <= 1'b0;
                        end
                        else begin
                            op  <= op1;
                            op1 <= op2;
                            op2 <= ins_0_in;
                        end
                    end
                    2'd2 : begin
                        if(not_branched == 1'b1) begin
                            op  <= op2;
                            op1 <= temp_ir4;
                            op2 <= temp_ir5;
                            not_branched <= 1'b0;
                        end
                        else begin
                            op  <=  op2;
                            op1 <= ins_0_in;
                            op2 <= ins_1_in;
                        end
                    end
                    2'd3 : begin
                        if(not_branched == 1'b1) begin
                            op  <= temp_ir4;
                            op1 <= temp_ir5;
                            op2 <= temp_ir6;
                            not_branched <= 1'b0;
                        end
                        else begin
                            op <=  ins_0_in;
                            op1 <= ins_1_in;
                            op2 <= ins_2_in;
                        end
                    end
                endcase
                
            end
            
        end 
    end
    
    // always @(*) begin
        // if(control_branchz_in == 1'b0) begin
            // op_out  = op;
            // op1_out = op1;
            // op2_out = op2;
        // end
        // else begin
            // if(cz_flag_in == 1'b1) begin
                // op_out  = op;
                // op1_out = op1;
                // op2_out = op2;
            // end
            // else begin
                // op_out  = temp_ir1;
                // op1_out = temp_ir2;
                // op2_out = temp_ir3;
            // end
        // end
    // end
    
    // always@(posedge clk) begin
        // if(p_mem_in == 1'b0) begin
            // temp_ir6 <= ins_2_in;
            // temp_ir5 <= ins_1_in;
            // temp_ir4 <= ins_0_in;
        // end
    // end


endmodule
