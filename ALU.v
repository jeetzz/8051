`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:56:59 10/22/2013 
// Design Name: 
// Module Name:    ALU 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ALU(
        clk,
        
		in0, 
		in1,
		ALU_bit, 
		out, 
		PSW_in, 
		PSW_out, 
		branch, 
		z, 
		op
    );
	 `include "def.v"
	 input [7:0] in0, in1;
	 input [2:0] ALU_bit;
	 output reg[7:0] out;
	 input [5:0] op;
	 input [2:0] PSW_in;
	 output [2:0] PSW_out;
	 input[2:0] branch;
	 output reg z;
	 input clk;
	 
	 reg c, ac, ov;
	 wire c_in, ac_in, ov_in;
	 
	 reg [7:0] temp;
	// reg DPTR_C;
	 
	 assign {c_in,ac_in,ov_in} = PSW_in;
	 assign PSW_out = {c,ac,ov};
	 
	 wire [8:0] add, add_c, add_1;
	 assign add = in0 + in1;
	 assign add_c = in0 + in1+ c_in;
	 assign add_1 = in1+ 8'b1;
	 
	 wire [7:0] subb;
	 assign subb = in0-in1-c_in;
	 
	 wire [15:0] mul;
	 //assign mul = {in0,in1};//in0*in1;
	 assign mul = in0*in1;
	 
	 wire in1_bit;
	 assign in1_bit = in1[ALU_bit];
	 
	 wire [7:0] mask1_0, mask0_1, mask1_C;
	 assign mask1_0 = ~(mask0_1);
	 assign mask0_1 = 8'h00 | (1'b1 << ALU_bit);
	 assign mask1_C = 8'hFF & (c_in << ALU_bit);
	 
	 wire [8:0] DA_int, DA_out;
	 assign DA_int = (ac_in | in1[3:0] > 8'd9) ?  in1 + 8'd6 : {1'b0, in1};
	 assign DA_out = ( c_in | DA_int[8] | in1[7:4] > 8'd8) ? DA_int + 8'h60 : {DA_int} ;
	 
	
	 
	always @ (*) begin
		 case(op) 
			0:begin
				out = in0; c = c_in; ac = ac_in; ov = ov_in; 
			end
			1:begin
				out = in1; c = c_in; ac = ac_in; ov = ov_in; 
			end
			2:begin
				out = add[7:0]; c = add[8]; ac = in0[4]^in1[4]^add[4];  ov = (in0[7]^in1[7]^add[7])~^(add[8]) ; // carry out from 6 or 7 but not both
			end
			3:begin
				out = add_c[7:0]; c = add_c[8]; ac = in0[4]^in1[4]^add_c[4];  ov = (in0[7]^in1[7]^add_c[7])~^(add_c[8]) ; 
			end
			4:begin
				out = in0 & in1; c = c_in; ac = ac_in; ov = ov_in; 
			end
			5:begin
				out = in0 | in1; c = c_in; ac = ac_in; ov = ov_in; 
			end
			6:begin
				out = in0 ^ in1; c = c_in; ac = ac_in; ov = ov_in; 
			end
			7:begin
				out = 8'hxx; c = c_in & in1_bit  ; ac = ac_in; ov = ov_in; 
			end
			8:begin
				out = 8'hxx; c = c_in & ~in1_bit  ; ac = ac_in; ov = ov_in; 
			end
			9:begin
				out = 8'hxx; c = c_in | in1_bit  ; ac = ac_in; ov = ov_in; 
			end
			10:begin
				out = 8'hxx; c = c_in | ~in1_bit  ; ac = ac_in; ov = ov_in; 
			end
			11:begin
				out = 8'hxx; c = in0 < in1  ; ac = ac_in; ov = ov_in; 
			end
			12:begin
				out = 8'hxx; c = in1 < in0  ; ac = ac_in; ov = ov_in; 
			end
			13:begin
				out = ~in1; c = c_in; ac = ac_in; ov = ov_in; 
			end
			14:begin
				out = in1 & mask1_0; c = c_in; ac = ac_in; ov = ov_in; 
			end
			15:begin
				out = in1 ^ mask0_1; c = c_in; ac = ac_in; ov = ov_in; 
			end
			16:begin
				out = in1 | mask0_1; c = c_in; ac = ac_in; ov = ov_in; 
			end
			17:begin
				out = 8'hxx; c = 1'b1  ; ac = ac_in; ov = ov_in; 
			end
			18:begin
				out = 8'hxx; c = 1'b0  ; ac = ac_in; ov = ov_in; 
			end
			19:begin
				out = 8'hxx; c = ~c_in  ; ac = ac_in; ov = ov_in; 
			end
			20:begin
				out = DA_out[7:0]; c= (c_in|DA_int[8] | DA_out[8]); ac= ac_in; ov=ov_in;
			end
			21:begin
				out = in1 -8'b1; c = c_in  ; ac = ac_in; ov = ov_in; 
			end
			22:begin
				out = add_1[7:0]; c = c_in  ; ac = ac_in; ov = ov_in; 
			end
			23:begin
				out = in1 ^ mask1_C; c = c_in; ac = ac_in; ov = ov_in; 
			end
			24:begin
				out = {in1[6:0], in1[7]}; c = c_in  ; ac = ac_in; ov = ov_in; 
			end
			25:begin
				out = {in1[6:0], c_in}; c = in1[7]  ; ac = ac_in; ov = ov_in; 
			end
			26:begin
				out = {in1[0], in1[7:1]}; c = c_in  ; ac = ac_in; ov = ov_in; 
			end
			27:begin
				out = {c_in, in1[7:1]}; c = in1[0]  ; ac = ac_in; ov = ov_in; 
			end
			28:begin
				out = {in1[3:0], in1[7:4]}; c = c_in  ; ac = ac_in; ov = ov_in; 
			end
			29:begin
				out = in0; c = c_in  ; ac = ac_in; ov = ov_in; 
			end
			30:begin
				out = {in1[7:4], in0[3:0]}; c = c_in  ; ac = ac_in; ov = ov_in; 
			end
			31:begin
				out = temp; c = c_in; ac = ac_in; ov = ov_in; 
			end
			32:begin
				out = subb; c = (in0 < in1); ac = (in0[3:0] < in1[3:0]); ov =( (in0 < in1) ^(in0[6:0] < in1[6:0]) );
			end
			33: begin
				out = 8'bxx; c = in1_bit; ac = ac_in; ov = ov_in;
			end
			34: begin
				out = in1+ temp; c = c_in; ac = ac_in; ov = ov_in;
			end
			35: begin
				out = 8'd0; c = c_in; ac = ac_in; ov = ov_in;
			end
			
			36:begin
				out = mul[7:0] ; c = 1'b0; ac = ac_in; ov = (mul[15:8] != 8'h00); 
			end
			default: begin
				out = in1; c = c_in; ac = ac_in; ov = ov_in;
			end
		 endcase
	end
	
	always @(*) begin
		case (branch) 
			`ALUB_B  : z = in1_bit;
			`ALUB_NE : z = (in1 != in0);
			`ALUB_OZ : z = (out==0);
			`ALUB_ONZ: z = (out!=0);
			`ALUB_C  : z = (c_in);
			`ALUB_NC : z = (~c_in);
			`ALUB_T  : z = (1'b1);
			`ALUB_NB : z = ~in1_bit;
		endcase
	end
	
	always @ (posedge clk) begin
		case (op)
			22: temp <= {7'd0 ,add_1[8]};
			29: temp <= in1;
			30: temp <= {in0[7:4], in1[3:0]};
			36: temp <= mul[15:8];
			default: temp <= 8'hxx;
		endcase
	end
endmodule
