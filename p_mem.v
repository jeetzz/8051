`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:38:57 10/15/2013 
// Design Name: 
// Module Name:    p_mem 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module p_mem
    (
        clk,
        reset,
        
        portw_addr,
        portw_data,
        portw_en,
        
        portr1_addr,
        portr1_ins0,
        portr1_ins1,
        portr1_ins2,
        portr1_en,
        
        portr2_addr,
        portr2_data,
        portr2_en
    );
    
    
//-------------------------------------------------------
//LOCALPARAMS
//-------------------------------------------------------
    
    localparam  LOG2NUM_OF_BANKS        = 2;
    localparam  INSTRUCTION_WIDTH       = 8;
    localparam  P_ADDR_WIDTH            = 16;
    localparam  MEM_SIZE                = 1 << (P_ADDR_WIDTH-LOG2NUM_OF_BANKS);
    localparam  NUM_OF_BANKS            = 1 << LOG2NUM_OF_BANKS;
      
//-------------------------------------------------------
// I/O Signals
//--------------------------------------------------------

    input                               clk;
    input                               reset;
    
    input       [P_ADDR_WIDTH-1:0]      portw_addr;
    input       [INSTRUCTION_WIDTH-1:0] portw_data;
    input                               portw_en;
    
    input       [P_ADDR_WIDTH-1:0]      portr1_addr;
    output reg  [INSTRUCTION_WIDTH-1:0] portr1_ins0;
    output reg  [INSTRUCTION_WIDTH-1:0] portr1_ins1;
    output reg  [INSTRUCTION_WIDTH-1:0] portr1_ins2;
    input                               portr1_en;
    
    input       [P_ADDR_WIDTH-1:0]      portr2_addr;
    output reg  [INSTRUCTION_WIDTH-1:0] portr2_data;
    input                               portr2_en;
    
//--------------------------------------------------------
// Internal Regs and wires
//--------------------------------------------------------
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank0_portw_addr;
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank0_portr1_addr;
    wire        [INSTRUCTION_WIDTH - 1:0]               p_mem_bank0_r1data;
    wire        [INSTRUCTION_WIDTH - 1:0]               p_mem_bank0_r2data;
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank0_portr2_addr;
    
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank1_portw_addr;
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank1_portr1_addr;
    wire        [INSTRUCTION_WIDTH - 1:0]               p_mem_bank1_r1data;
    wire        [INSTRUCTION_WIDTH - 1:0]               p_mem_bank1_r2data;
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank1_portr2_addr;
    
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank2_portw_addr;
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank2_portr1_addr;
    wire        [INSTRUCTION_WIDTH - 1:0]               p_mem_bank2_r1data;
    wire        [INSTRUCTION_WIDTH - 1:0]               p_mem_bank2_r2data;
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank2_portr2_addr;
 
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank3_portw_addr;
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank3_portr1_addr;
    wire        [INSTRUCTION_WIDTH - 1:0]               p_mem_bank3_r1data;
    wire        [INSTRUCTION_WIDTH - 1:0]               p_mem_bank3_r2data;
    reg         [P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1:0] p_mem_bank3_portr2_addr;
    
    reg         [NUM_OF_BANKS - 1:0]                    p_mem_portw_banksel;
    reg         [NUM_OF_BANKS - 1:0]                    p_mem_portr1_banksel;
    reg         [NUM_OF_BANKS - 1:0]                    p_mem_portr2_banksel;
    
    reg         [LOG2NUM_OF_BANKS-1:0]                  portr1_addr_low2b_reg;
    reg         [LOG2NUM_OF_BANKS-1:0]                  portr2_addr_low2b_reg;            


//--------------------------------------------------------
// Implementation
//--------------------------------------------------------
    
    always @(*) begin
        case(portw_addr[1:0])
            2'b00 : begin
                p_mem_portw_banksel     = 4'b0001;
                p_mem_bank0_portw_addr  = portw_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank1_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank2_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank3_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};                
            end
            2'b01 : begin
                p_mem_portw_banksel     = 4'b0010;
                p_mem_bank0_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank1_portw_addr  = portw_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank2_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank3_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
            end 
            2'b10 : begin
                p_mem_portw_banksel     = 4'b0100;
                p_mem_bank0_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank1_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank2_portw_addr  = portw_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank3_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
            end 
            2'b11 : begin
                p_mem_portw_banksel     = 4'b1000;
                p_mem_bank0_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank1_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank2_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank3_portw_addr  = portw_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
            end 
//            default : begin
//                p_mem_portw_banksel     = 3'b0000;
//                p_mem_bank0_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
//                p_mem_bank1_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
//                p_mem_bank2_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
//                p_mem_bank3_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
//            end
        endcase
        
        case(portr1_addr[1:0])
            2'b00 : begin
                p_mem_portr1_banksel    = 4'b0111;
                p_mem_bank0_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank1_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank2_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank3_portr1_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
            end
            2'b01 : begin
                p_mem_portr1_banksel    = 4'b1110;
                p_mem_bank0_portr1_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank1_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank2_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank3_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
            end
            2'b10 : begin
                p_mem_portr1_banksel    = 4'b1101;
                p_mem_bank0_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS] + {{(P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1){1'b0}},1'b1};
                p_mem_bank1_portr1_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank2_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank3_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
            end
            2'b11 : begin
                p_mem_portr1_banksel    = 4'b1011;
                p_mem_bank0_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS] + {{(P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1){1'b0}},1'b1};
                p_mem_bank1_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS] + {{(P_ADDR_WIDTH - LOG2NUM_OF_BANKS - 1){1'b0}},1'b1};
                p_mem_bank2_portr1_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank3_portr1_addr  = portr1_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
            end
        endcase
        
        case(portr2_addr[1:0])
            2'b00 : begin
                p_mem_portr2_banksel     = 4'b0001;
                p_mem_bank0_portr2_addr  = portr2_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank1_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank2_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank3_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};

            end
            2'b01 : begin
                p_mem_portr2_banksel     = 4'b0010;
                p_mem_bank0_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank1_portr2_addr  = portr2_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank2_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank3_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};

            end 
            2'b10 : begin
                p_mem_portr2_banksel     = 4'b0100;
                p_mem_bank0_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank1_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank2_portr2_addr  = portr2_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];
                p_mem_bank3_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};

            end 
            2'b11 : begin
                p_mem_portr2_banksel     = 4'b1000;
                p_mem_bank0_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank1_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank2_portr2_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
                p_mem_bank3_portr2_addr  = portr2_addr[P_ADDR_WIDTH-1:LOG2NUM_OF_BANKS];

            end 
//            default : begin
//                p_mem_portw_banksel     = 3'b0000;
//                p_mem_bank0_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
//                p_mem_bank1_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
//                p_mem_bank2_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
//                p_mem_bank3_portw_addr  = {(P_ADDR_WIDTH - LOG2NUM_OF_BANKS){1'bx}};
//            end
        endcase
    end
    
    always @(posedge clk) begin
            portr1_addr_low2b_reg  <= portr1_addr[LOG2NUM_OF_BANKS - 1:0];
            portr2_addr_low2b_reg  <= portr2_addr[LOG2NUM_OF_BANKS - 1:0];
    end
    
    
    always@(*) begin
        case(portr2_addr_low2b_reg)
            2'b00 : begin                
                portr2_data              = p_mem_bank0_r2data;
            end
            2'b01 : begin                
                portr2_data              = p_mem_bank1_r2data;
            end
            2'b10 : begin                
                portr2_data              = p_mem_bank2_r2data;
            end
            2'b11 : begin                
                portr2_data              = p_mem_bank3_r2data;
            end
        endcase
    end
    
    
    always @(*) begin
        case(portr1_addr_low2b_reg)
            2'b00 : begin                
                portr1_ins0             = p_mem_bank0_r1data;
                portr1_ins1             = p_mem_bank1_r1data;
                portr1_ins2             = p_mem_bank2_r1data;
            end
            2'b01 : begin                
                portr1_ins0             = p_mem_bank1_r1data;
                portr1_ins1             = p_mem_bank2_r1data;
                portr1_ins2             = p_mem_bank3_r1data;
            end
            2'b10 : begin                
                portr1_ins0             = p_mem_bank2_r1data;
                portr1_ins1             = p_mem_bank3_r1data;
                portr1_ins2             = p_mem_bank0_r1data;
            end
            2'b11 : begin                
                portr1_ins0             = p_mem_bank3_r1data;
                portr1_ins1             = p_mem_bank0_r1data;
                portr1_ins2             = p_mem_bank1_r1data;
            end
        endcase
    end

    p_mem_bank p_mem_bank0
    (
        .clk            (clk),
        .reset          (reset),
        
        .portw_addr     (p_mem_bank0_portw_addr),
        .portw_data     (portw_data),
        .portw_en       (p_mem_portw_banksel[0] & portw_en),
        
        .portr1_addr    (p_mem_bank0_portr1_addr),
        .portr1_data    (p_mem_bank0_r1data),
        .portr1_en      (p_mem_portr1_banksel[0] & portr1_en),
        
        .portr2_addr    (p_mem_bank0_portr2_addr),
        .portr2_data    (p_mem_bank0_r2data),
        .portr2_en      (p_mem_portr2_banksel[0] & portr2_en)
    );
    
    p_mem_bank p_mem_bank1
    (
        .clk             (clk),
        .reset           (reset),
        
        .portw_addr      (p_mem_bank1_portw_addr),
        .portw_data      (portw_data),
        .portw_en        (p_mem_portw_banksel[1] & portw_en),
                        
        .portr1_addr     (p_mem_bank1_portr1_addr),
        .portr1_data     (p_mem_bank1_r1data),
        .portr1_en       (p_mem_portr1_banksel[1] & portr1_en),
                        
        .portr2_addr     (p_mem_bank1_portr2_addr),
        .portr2_data     (p_mem_bank1_r2data),
        .portr2_en       (p_mem_portr2_banksel[1] & portr2_en)
    );
    
    p_mem_bank p_mem_bank2
    (
        .clk             (clk),
        .reset           (reset),
        
        .portw_addr      (p_mem_bank2_portw_addr),
        .portw_data      (portw_data),
        .portw_en        (p_mem_portw_banksel[2] & portw_en),
                        
        .portr1_addr     (p_mem_bank2_portr1_addr),
        .portr1_data     (p_mem_bank2_r1data),
        .portr1_en       (p_mem_portr1_banksel[2] & portr1_en),
                        
        .portr2_addr     (p_mem_bank2_portr2_addr),
        .portr2_data     (p_mem_bank2_r2data),
        .portr2_en       (p_mem_portr2_banksel[2] & portr2_en)
    );
    
    p_mem_bank p_mem_bank3
    (
        .clk             (clk),
        .reset           (reset),
        
        .portw_addr      (p_mem_bank3_portw_addr),
        .portw_data      (portw_data),
        .portw_en        (p_mem_portw_banksel[3] & portw_en),
                        
        .portr1_addr     (p_mem_bank3_portr1_addr),
        .portr1_data     (p_mem_bank3_r1data),
        .portr1_en       (p_mem_portr1_banksel[3] & portr1_en),
                        
        .portr2_addr     (p_mem_bank3_portr2_addr),
        .portr2_data     (p_mem_bank3_r2data),
        .portr2_en       (p_mem_portr2_banksel[3] & portr2_en)
    );



endmodule
