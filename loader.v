`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:11:31 10/14/2013 
// Design Name: 
// Module Name:    loader 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module loader(
        clk,
        reset,
        
        pc_in,
        pc_out,
        pc_offset_sel_in,
        branching_in,
        
        
        op1_2d_in,
        op1_1d_in,
        op2_2d_in,
        op2_1d_in,
        
        p_addr_out
    );
    
//-------------------------------------------------------
// Global Includes
//-------------------------------------------------------   
    `include "def.v"
   
//-------------------------------------------------------
// LOCALPARAMS
//-------------------------------------------------------

    localparam  INSTRUCTION_WIDTH = 8;
    localparam  P_ADDR_WIDTH = 16;
    localparam  PC_OFFSEL_SEL_WIDTH = 4;
    
    
    
    // PC_OFFSET_SEL_OPTIONS
    localparam  [PC_OFFSEL_SEL_WIDTH - 1:0] PC_OFFSEL_SEL_ZERO      = `OFF_0;  
    localparam  [PC_OFFSEL_SEL_WIDTH - 1:0] PC_OFFSEL_SEL_THREE     = `OFF_3;
    localparam  [PC_OFFSEL_SEL_WIDTH - 1:0] PC_OFFSEL_SEL_FOUR      = `OFF_4;
    localparam  [PC_OFFSEL_SEL_WIDTH - 1:0] PC_OFFSEL_SEL_FIVE      = `OFF_5;
    localparam  [PC_OFFSEL_SEL_WIDTH - 1:0] PC_OFFSEL_SEL_SIX       = `OFF_6;
    localparam  [PC_OFFSEL_SEL_WIDTH - 1:0] PC_OFFSEL_SEL_OP1_2D    = `OFF_OP12D;  
    localparam  [PC_OFFSEL_SEL_WIDTH - 1:0] PC_OFFSEL_SEL_OP1_1D    = `OFF_OP11D;
    localparam  [PC_OFFSEL_SEL_WIDTH - 1:0] PC_OFFSEL_SEL_OP2_2D    = `OFF_OP22D;
    localparam  [PC_OFFSEL_SEL_WIDTH - 1:0] PC_OFFSEL_SEL_OP2_1D    = `OFF_OP21D;
    
//-------------------------------------------------------
// I/O Signals
//--------------------------------------------------------

    input   clk;
    input   reset;
    
    input   [P_ADDR_WIDTH - 1:0]        pc_in;
    output  [P_ADDR_WIDTH - 1:0]        pc_out;
    input   [PC_OFFSEL_SEL_WIDTH - 1:0] pc_offset_sel_in;
    input                               branching_in;
    
    
    input   [INSTRUCTION_WIDTH - 1:0]   op1_2d_in;
    input   [INSTRUCTION_WIDTH - 1:0]   op1_1d_in;
    input   [INSTRUCTION_WIDTH - 1:0]   op2_2d_in;
    input   [INSTRUCTION_WIDTH - 1:0]   op2_1d_in;
    
    output  [P_ADDR_WIDTH - 1:0]        p_addr_out;
    
//--------------------------------------------------------
// Internal Regs and wires
//--------------------------------------------------------

    reg     [P_ADDR_WIDTH - 1:0]        prev_addr;
    
    reg     [P_ADDR_WIDTH - 1:0]        pc_offset_int;
    
//--------------------------------------------------------
// Implementation
//--------------------------------------------------------

    always @(*) begin : pc_offset_sel_mux
        case(pc_offset_sel_in)
            PC_OFFSEL_SEL_ZERO      :  pc_offset_int = {P_ADDR_WIDTH{1'b0}};
            PC_OFFSEL_SEL_THREE     :  pc_offset_int = {{(P_ADDR_WIDTH-PC_OFFSEL_SEL_WIDTH){1'b0}},4'd3};
            PC_OFFSEL_SEL_FOUR      :  pc_offset_int = {{(P_ADDR_WIDTH-PC_OFFSEL_SEL_WIDTH){1'b0}},4'd4};
            PC_OFFSEL_SEL_FIVE      :  pc_offset_int = {{(P_ADDR_WIDTH-PC_OFFSEL_SEL_WIDTH){1'b0}},4'd5};
            PC_OFFSEL_SEL_SIX       :  pc_offset_int = {{(P_ADDR_WIDTH-PC_OFFSEL_SEL_WIDTH){1'b0}},4'd6};
            PC_OFFSEL_SEL_OP1_2D    :  pc_offset_int = {{(P_ADDR_WIDTH-INSTRUCTION_WIDTH){op1_2d_in[INSTRUCTION_WIDTH-1]}},op1_2d_in};
            PC_OFFSEL_SEL_OP1_1D    :  pc_offset_int = {{(P_ADDR_WIDTH-INSTRUCTION_WIDTH){op1_1d_in[INSTRUCTION_WIDTH-1]}},op1_1d_in};
            PC_OFFSEL_SEL_OP2_2D    :  pc_offset_int = {{(P_ADDR_WIDTH-INSTRUCTION_WIDTH){op2_2d_in[INSTRUCTION_WIDTH-1]}},op2_2d_in};
            PC_OFFSEL_SEL_OP2_1D    :  pc_offset_int = {{(P_ADDR_WIDTH-INSTRUCTION_WIDTH){op2_1d_in[INSTRUCTION_WIDTH-1]}},op2_1d_in};
            default                 :  pc_offset_int = {P_ADDR_WIDTH{1'bx}};
        endcase
    end
    
            
    always @(posedge clk) begin
        if(reset) begin
            prev_addr <= {P_ADDR_WIDTH{1'b0}};
        end
        else begin
            prev_addr <= p_addr_out;
        end
    end
    
    assign p_addr_out = (branching_in) ? prev_addr + pc_offset_int : pc_in + pc_offset_int;
    assign pc_out     = prev_addr;
    
endmodule
