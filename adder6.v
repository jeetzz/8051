`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:51:29 10/23/2013 
// Design Name: 
// Module Name:    adder6 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module adder16
    (
        clk,
        
        acc_in,
        dptr_in,
        pc_in,
        
        addr16_sel_in,
        
        ans_out,
        ans_out_reg
    );
    
//-------------------------------------------------------
// GLOBAL INCLUDES
//-------------------------------------------------------
    
    	`include "def.v"
        
        
//-------------------------------------------------------
// LOCALPARAMS
//-------------------------------------------------------
    
    localparam P_ADDR_WIDTH             = 16;
    localparam INSTRUCTION_WIDTH        = 8;
    
//--------------------------------------------------------
// I/O Signals
//--------------------------------------------------------    
    
    input                                   clk;
    input       [INSTRUCTION_WIDTH - 1:0]   acc_in;
    input       [P_ADDR_WIDTH - 1:0]        dptr_in;
    input       [P_ADDR_WIDTH - 1:0]        pc_in;
    input                                   addr16_sel_in;
    
    output reg  [P_ADDR_WIDTH - 1:0]        ans_out;
    output reg  [P_ADDR_WIDTH - 1:0]        ans_out_reg;
    
    always @(*) begin
        case(addr16_sel_in) 
            `ADD16_PC: begin
                ans_out = {{(P_ADDR_WIDTH - INSTRUCTION_WIDTH){1'b0}},acc_in} + pc_in;
            end
            `ADD16_DPTR: begin
                ans_out = {{(P_ADDR_WIDTH - INSTRUCTION_WIDTH){1'b0}},acc_in} + dptr_in;
            end
            default: begin
                ans_out = {P_ADDR_WIDTH{1'bx}};
            end
        endcase

    end
    
    always @(posedge clk) begin
        ans_out_reg <= ans_out;
    end
    
    

endmodule
