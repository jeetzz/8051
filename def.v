`define SFR_P0      now_mem_128
`define SFR_P1      now_mem_144
`define SFR_P2      now_mem_160
`define SFR_P3      now_mem_176
`define SFR_TCON    now_mem_136
`define SFR_TMOD    now_mem_137
`define SFR_TL0     now_mem_138
`define SFR_TL1     now_mem_139
`define SFR_TH0     now_mem_140
`define SFR_TH1     now_mem_141
`define SFR_SCON    now_mem_152
`define SFR_SBUF    now_mem_153
`define SFR_IE      now_mem_168
`define SFR_IP      now_mem_184
`define SFR_ACC     now_mem_224
`define SFR_R0B0    now_mem_00
`define SFR_R0B1    now_mem_08
`define SFR_R0B2    now_mem_16
`define SFR_R0B3    now_mem_24
`define SFR_R1B0    now_mem_01
`define SFR_R1B1    now_mem_09
`define SFR_R1B2    now_mem_17
`define SFR_PSW     now_mem_208
`define SFR_R1B3    now_mem_25
`define SFR_SP      now_mem_129
`define SFR_B       now_mem_81
`define SFR_DPL     now_mem_130
`define SFR_DPH     now_mem_131



`define INC_0 2'b00
`define INC_1 2'b01
`define INC_2 2'b10
`define INC_3 2'b11
`define INC_x `INC_0

`define SHIFT_0 2'b00
`define SHIFT_1 2'b01
`define SHIFT_2 2'b10
`define SHIFT_3 2'b11
`define SHIFT_x `SHIFT_0

`define OFF_0 4'b0000
`define OFF_3 4'b0001
`define OFF_6 4'b0010
`define OFF_OP11D 4'b0100
`define OFF_OP12D 4'b0101
`define OFF_OP21D 4'b0110
`define OFF_OP22D 4'b0111
`define OFF_4 4'b1000
`define OFF_5 4'b1001
`define OFF_x `OFF_6

`define D0L0_P 2'b00
`define D0L0_OP1 2'b01
`define D0L0_OP2 2'b10
`define D0L0_x 2'bxx

`define D1L0_P 2'b00
`define D1L0_D1 2'b11
`define D1L0_OP1 2'b01
`define D1L0_OP2 2'b10
`define D1L0_x 2'bxx

`define RADD_R0 4'b0000
`define RADD_R1 4'b0001
`define RADD_OP1 4'b0010
`define RADD_Rn 4'b0011
`define RADD_ACC 4'b0100
`define RADD_OP2 4'b0101
`define RADD_SP 4'b0110
`define RADD_DPTRL 4'b0111
`define RADD_DPTRH 4'b1000
`define RADD_B 4'b1001
`define RADD_x 4'bxxxx

`define RBIT_T 1'b1
`define RBIT_F 1'b0
`define RBIT_x `RBIT_F

`define RIN_LATCH 1'b0
`define RIN_PORT 1'b1
`define RIN_x `RIN_PORT

`define D0L1_D 1'b1
`define D0L1_M 1'b0
`define D0L1_x 1'bx

`define D1L1_D 1'b1
`define D1L1_M 1'b0
`define D1L1_x 1'bx

`define ALU_0 6'd0
`define ALU_1 6'd1
`define ALU_2 6'd2
`define ALU_3 6'd3
`define ALU_4 6'd4
`define ALU_5 6'd5
`define ALU_6 6'd6
`define ALU_7 6'd7
`define ALU_8 6'd8
`define ALU_9 6'd9
`define ALU_10 6'd10
`define ALU_11 6'd11
`define ALU_12 6'd12
`define ALU_13 6'd13
`define ALU_14 6'd14
`define ALU_15 6'd15
`define ALU_16 6'd16
`define ALU_17 6'd17
`define ALU_18 6'd18
`define ALU_19 6'd19
`define ALU_20 6'd20
`define ALU_21 6'd21
`define ALU_22 6'd22
`define ALU_23 6'd23
`define ALU_24 6'd24
`define ALU_25 6'd25
`define ALU_26 6'd26
`define ALU_27 6'd27
`define ALU_28 6'd28
`define ALU_29 6'd29
`define ALU_30 6'd30
`define ALU_31 6'd31
`define ALU_32 6'd32
`define ALU_33 6'd33
`define ALU_34 6'd34
`define ALU_35 6'd35
`define ALU_36 6'd36
`define ALU_37 6'd37
`define ALU_x `ALU_1

`define ALUB_B 3'd0
`define ALUB_NE 3'd1
`define ALUB_OZ 3'd2
`define ALUB_ONZ 3'd3
`define ALUB_C 3'd4
`define ALUB_NC 3'd5
`define ALUB_T 3'd6
`define ALUB_NB 3'd7
`define ALUB_x 3'bxxx

`define WSEL_ALU 1'b0
`define WSEL_PM 1'b1
`define WSEL_x `WSEL_ALU

`define WADD_R0 4'b0000
`define WADD_R1 4'b0001
`define WADD_OP1 4'b0010
`define WADD_Rn 4'b0011
`define WADD_ACC 4'b0100
`define WADD_OP2 4'b0101
`define WADD_SP1 4'b0110
`define WADD_DPTRL 4'b0111
`define WADD_DPTRH 4'b1000
`define WADD_B 4'b1001
`define WADD_x 4'd10

`define WBIT_T 1'b1
`define WBIT_F 1'b0
`define WBIT_x `WBIT_F

`define TRUE 1'b1
`define FALSE 1'b0

`define SPDEC_T 1'b1
`define SPDEC_F 1'b0
`define SPDEC_x `SPDEC_F

`define PCL_P 	3'd0
`define PCL_OP1 3'd1
`define PCL_OP2 3'd2
`define PCL_ALU 3'd3
`define PCL_A16 3'd4
`define PCL_PCD 3'd5
`define PCL_x 	`PCL_P

`define PCH_P 	3'd0
`define PCH_OP 	3'd1
`define PCH_OP1 3'd2
`define PCH_ALU 3'd3
`define PCH_A16 3'd4
`define PCH_PCD 3'd5
`define PCH_x 	`PCH_P

`define POUT_Y 1'b0
`define POUT_OP4 1'b1
`define POUT_x `POUT_OP4

`define BZ_T 1'b1
`define BZ_F 1'b0
`define BZ_x `BZ_F

`define ADD16_PC 1'b0
`define ADD16_DPTR 1'b1
`define ADD16_x 1'bx

`define RETI_T 1'b1
`define RETI_F 1'b0
`define RETI_x `RETI_F