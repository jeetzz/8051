`timescale 1ns / 1ps


module mem_file
(
    clk,
	reset,
    op1_1d_in,
    op2_1d_in,
	op_1d_3LSB_in,
	//mr_addr0_sel,
	mr_addr1_sel_in,
	//mr_addr0_bit,
	mr_addr1_bit_in,
	mem_write_en_in,
    mem_write_bit_in,
    op1_2d_in,
    op2_2d_in,
    op_2d_3LSB_in,
    mem_write_sel_in,
    mem_w_addr_sel_in,
    mw_data_in,
    pm_data_in,
    psw_in,
    psw_out,
    sp_dec_in,
    r_data0_out,
    r_data1_out,
    dptr_out,
    acc_out,
    port0_out,
    port1_out,
    port2_out,
    port3_out,
    port0_in,
    port1_in,
    port2_in,
    port3_in,
    rxdata_serial_in,
    serial_rx_done_in,
    serial_ren_out,
    serial_ri_flag_out,
    
    txdata_serial_out,
    serial_tx_done_in,
    serial_ti_flag_out,
    serial_enable_out
    
    
    //io_port_en_in

);


    `include    "def.v"
    //---------------------------------------------------------------------------------------------------------------------
    // parameter definitions
    //---------------------------------------------------------------------------------------------------------------------
    localparam  INSTRUCTION_WIDTH       = 8;
    
    parameter ACC_ADDR  = 8'd224;
    parameter R0B0_ADDR = 8'd00;
    parameter R0B1_ADDR = 8'd08;
    parameter R0B2_ADDR = 8'd16;
    parameter R0B3_ADDR = 8'd24;
    parameter R1B0_ADDR = 8'd01;
    parameter R1B1_ADDR = 8'd09;
    parameter R1B2_ADDR = 8'd17;
    parameter R1B3_ADDR = 8'd25;
    parameter PSW_ADDR  = 8'd208;
    parameter SP_ADDR   = 8'd129;
    parameter B_ADDR    = 8'd240;
    parameter DPL_ADDR  = 8'd130;
    parameter DPH_ADDR  = 8'd131;
    
    localparam P0_ADDR      =8'd128;
    localparam P1_ADDR      =8'd144;
    localparam P2_ADDR      =8'd160;
    localparam P3_ADDR      =8'd176;
    localparam TCON_ADDR    =8'd136;
    localparam TMOD_ADDR    =8'd137;
    localparam TL0_ADDR     =8'd138;
    localparam TL1_ADDR     =8'd139;
    localparam TH0_ADDR     =8'd140;
    localparam TH1_ADDR     =8'd141;
    localparam SCON_ADDR    =8'd152;
    localparam SBUF_ADDR    =8'd153;
    localparam IE_ADDR      =8'd168;
    localparam IP_ADDR      =8'd184;    
    

	
    //---------------------------------------------------------------------------------------------------------------------
    // localparam definitions
    //---------------------------------------------------------------------------------------------------------------------
    
    localparam B_SEL_0 = 0;
    localparam B_SEL_1 = 1;
    localparam B_SEL_2 = 2;
    localparam B_SEL_3 = 3;
     
    
    localparam MR_AD0_BIT       = 1;
    
    localparam MR_AD1_BIT_L = 2;
    localparam MR_AD1_BIT_H = 3;
    
    
    localparam MW_DT_ALU = 0;
    localparam MW_DT_PM = 1;
    
    
    
    //---------------------------------------------------------------------------------------------------------------------
    // I/O signals
    //---------------------------------------------------------------------------------------------------------------------
    

    input                                   clk;
	input                                   reset;
    //input                                   io_port_en_in;
    input       [INSTRUCTION_WIDTH-1:0]     port0_in;
    input       [INSTRUCTION_WIDTH-1:0]     port1_in;
    input       [INSTRUCTION_WIDTH-1:0]     port2_in;
    input       [INSTRUCTION_WIDTH-1:0]     port3_in;
    
    output       [INSTRUCTION_WIDTH-1:0]    port0_out;
    output       [INSTRUCTION_WIDTH-1:0]    port1_out;
    output       [INSTRUCTION_WIDTH-1:0]    port2_out;
    output       [INSTRUCTION_WIDTH-1:0]    port3_out;
    
    
    input       [INSTRUCTION_WIDTH-1:0]     op1_1d_in;
    input       [INSTRUCTION_WIDTH-1:0]     op2_1d_in;
	input       [2:0]                       op_1d_3LSB_in;
//	input               mr_addr0_sel;
	input       [3:0]                       mr_addr1_sel_in;
//	input               mr_addr0_bit;
	input                                   mr_addr1_bit_in;
	input                                   mem_write_en_in;
    input                                   mem_write_bit_in;
    input       [INSTRUCTION_WIDTH-1:0]     op1_2d_in;
    input       [INSTRUCTION_WIDTH-1:0]     op2_2d_in;
    input       [2:0]                       op_2d_3LSB_in;
    input       [3:0]                       mem_w_addr_sel_in;
    input                                   mem_write_sel_in;
    input       [INSTRUCTION_WIDTH-1:0]     mw_data_in;
    input       [INSTRUCTION_WIDTH-1:0]     pm_data_in;
    input       [2:0]                       psw_in;
    input                                   sp_dec_in;
    output      [2:0]                       psw_out;
    output      [INSTRUCTION_WIDTH-1:0]     r_data0_out;
    output  reg [INSTRUCTION_WIDTH-1:0]     r_data1_out;
    output      [15:0]                      dptr_out;
    output      [INSTRUCTION_WIDTH-1:0]     acc_out;
    
    output                                  serial_enable_out;
    input       [INSTRUCTION_WIDTH-1:0]     rxdata_serial_in;
    input                                   serial_rx_done_in;
    output                                  serial_ren_out;
    output                                  serial_ri_flag_out;
    
    output      [INSTRUCTION_WIDTH-1:0]     txdata_serial_out;
    input                                   serial_tx_done_in;
    output                                  serial_ti_flag_out;
	
	//---------------------------------------------------------------------------------------------------------------------
    // Internal wires and registers
    //---------------------------------------------------------------------------------------------------------------------
    wire    io_port_en_in = 0;

    
    
    
    
	//reg			[7:0]       rd_mx_out0,r_addr0;
    reg			[7:0]       rd_mx_out1,r_addr1;
    reg			[7:0]       wr_mx_out,w_addr;
    wire        [7:0]       r0b0_r, r0b1_r, r0b2_r, r0b3_r, r1b0_r, r1b1_r, r1b2_r, r1b3_r;
    wire        [7:0]       r0b0_w, r0b1_w, r0b2_w, r0b3_w, r1b0_w, r1b1_w, r1b2_w, r1b3_w;
    wire        [1:0]       b_sel_r, b_sel_w;
    reg         [7:0]       r0_mux_in_r,r0_mux_in_w,r1_mux_in_r,r1_mux_in_w;
    
    reg         [7:0]       w_data;
    // wire        [7:0]       r_addr0_d;
    reg         [7:0]       r_addr1_d;
    // reg         [7:0]       R0_addr;
    // reg         [7:0]       R1_addr;
    
    wire                    parity_bit;
    
    wire [7:0] next_mem_0    ;
    wire [7:0] next_mem_1    ;
    wire [7:0] next_mem_2    ;
    wire [7:0] next_mem_3    ;
    wire [7:0] next_mem_4    ;
    wire [7:0] next_mem_5    ;
    wire [7:0] next_mem_6    ;
    wire [7:0] next_mem_7    ;
    wire [7:0] next_mem_8    ;
    wire [7:0] next_mem_9    ;
    wire [7:0] next_mem_10   ;
    wire [7:0] next_mem_11   ;
    wire [7:0] next_mem_12   ;
    wire [7:0] next_mem_13   ;
    wire [7:0] next_mem_14   ;
    wire [7:0] next_mem_15   ;
    wire [7:0] next_mem_16   ;
    wire [7:0] next_mem_17   ;
    wire [7:0] next_mem_18   ;
    wire [7:0] next_mem_19   ;
    wire [7:0] next_mem_20   ;
    wire [7:0] next_mem_21   ;
    wire [7:0] next_mem_22   ;
    wire [7:0] next_mem_23   ;
    wire [7:0] next_mem_24   ;
    wire [7:0] next_mem_25   ;
    wire [7:0] next_mem_26   ;
    wire [7:0] next_mem_27   ;
    wire [7:0] next_mem_28   ;
    wire [7:0] next_mem_29   ;
    wire [7:0] next_mem_30   ;
    wire [7:0] next_mem_31   ;
    wire [7:0] next_mem_32   ;
    wire [7:0] next_mem_33   ;
    wire [7:0] next_mem_34   ;
    wire [7:0] next_mem_35   ;
    wire [7:0] next_mem_36   ;
    wire [7:0] next_mem_37   ;
    wire [7:0] next_mem_38   ;
    wire [7:0] next_mem_39   ;
    wire [7:0] next_mem_40   ;
    wire [7:0] next_mem_41   ;
    wire [7:0] next_mem_42   ;
    wire [7:0] next_mem_43   ;
    wire [7:0] next_mem_44   ;
    wire [7:0] next_mem_45   ;
    wire [7:0] next_mem_46   ;
    wire [7:0] next_mem_47   ;
    wire [7:0] next_mem_48   ;
    wire [7:0] next_mem_49   ;
    wire [7:0] next_mem_50   ;
    wire [7:0] next_mem_51   ;
    wire [7:0] next_mem_52   ;
    wire [7:0] next_mem_53   ;
    wire [7:0] next_mem_54   ;
    wire [7:0] next_mem_55   ;
    wire [7:0] next_mem_56   ;
    wire [7:0] next_mem_57   ;
    wire [7:0] next_mem_58   ;
    wire [7:0] next_mem_59   ;
    wire [7:0] next_mem_60   ;
    wire [7:0] next_mem_61   ;
    wire [7:0] next_mem_62   ;
    wire [7:0] next_mem_63   ;
    wire [7:0] next_mem_64   ;
    wire [7:0] next_mem_65   ;
    wire [7:0] next_mem_66   ;
    wire [7:0] next_mem_67   ;
    wire [7:0] next_mem_68   ;
    wire [7:0] next_mem_69   ;
    wire [7:0] next_mem_70   ;
    wire [7:0] next_mem_71   ;
    wire [7:0] next_mem_72   ;
    wire [7:0] next_mem_73   ;
    wire [7:0] next_mem_74   ;
    wire [7:0] next_mem_75   ;
    wire [7:0] next_mem_76   ;
    wire [7:0] next_mem_77   ;
    wire [7:0] next_mem_78   ;
    wire [7:0] next_mem_79   ;
    wire [7:0] next_mem_80   ;
    wire [7:0] next_mem_81   ;
    wire [7:0] next_mem_82   ;
    wire [7:0] next_mem_83   ;
    wire [7:0] next_mem_84   ;
    wire [7:0] next_mem_85   ;
    wire [7:0] next_mem_86   ;
    wire [7:0] next_mem_87   ;
    wire [7:0] next_mem_88   ;
    wire [7:0] next_mem_89   ;
    wire [7:0] next_mem_90   ;
    wire [7:0] next_mem_91   ;
    wire [7:0] next_mem_92   ;
    wire [7:0] next_mem_93   ;
    wire [7:0] next_mem_94   ;
    wire [7:0] next_mem_95   ;
    wire [7:0] next_mem_96   ;
    wire [7:0] next_mem_97   ;
    wire [7:0] next_mem_98   ;
    wire [7:0] next_mem_99   ;
    wire [7:0] next_mem_100  ;
    wire [7:0] next_mem_101  ;
    wire [7:0] next_mem_102  ;
    wire [7:0] next_mem_103  ;
    wire [7:0] next_mem_104  ;
    wire [7:0] next_mem_105  ;
    wire [7:0] next_mem_106  ;
    wire [7:0] next_mem_107  ;
    wire [7:0] next_mem_108  ;
    wire [7:0] next_mem_109  ;
    wire [7:0] next_mem_110  ;
    wire [7:0] next_mem_111  ;
    wire [7:0] next_mem_112  ;
    wire [7:0] next_mem_113  ;
    wire [7:0] next_mem_114  ;
    wire [7:0] next_mem_115  ;
    wire [7:0] next_mem_116  ;
    wire [7:0] next_mem_117  ;
    wire [7:0] next_mem_118  ;
    wire [7:0] next_mem_119  ;
    wire [7:0] next_mem_120  ;
    wire [7:0] next_mem_121  ;
    wire [7:0] next_mem_122  ;
    wire [7:0] next_mem_123  ;
    wire [7:0] next_mem_124  ;
    wire [7:0] next_mem_125  ;
    wire [7:0] next_mem_126  ;
    wire [7:0] next_mem_127  ;
    wire [7:0] next_mem_128  ;
    reg  [7:0] next_mem_129  ;
    wire [7:0] next_mem_130  ;
    wire [7:0] next_mem_131  ;
    wire [7:0] next_mem_132  ;
    wire [7:0] next_mem_133  ;
    wire [7:0] next_mem_134  ;
    wire [7:0] next_mem_135  ;
    wire [7:0] next_mem_136  ;
    wire [7:0] next_mem_137  ;
    wire [7:0] next_mem_138  ;
    wire [7:0] next_mem_139  ;
    wire [7:0] next_mem_140  ;
    wire [7:0] next_mem_141  ;
    wire [7:0] next_mem_142  ;
    wire [7:0] next_mem_143  ;
    wire [7:0] next_mem_144  ;
    wire [7:0] next_mem_145  ;
    wire [7:0] next_mem_146  ;
    wire [7:0] next_mem_147  ;
    wire [7:0] next_mem_148  ;
    wire [7:0] next_mem_149  ;
    wire [7:0] next_mem_150  ;
    wire [7:0] next_mem_151  ;
    wire [7:0] next_mem_152  ;
    wire [7:0] next_mem_153  ;
    wire [7:0] next_mem_154  ;
    wire [7:0] next_mem_155  ;
    wire [7:0] next_mem_156  ;
    wire [7:0] next_mem_157  ;
    wire [7:0] next_mem_158  ;
    wire [7:0] next_mem_159  ;
    wire [7:0] next_mem_160  ;
    wire [7:0] next_mem_161  ;
    wire [7:0] next_mem_162  ;
    wire [7:0] next_mem_163  ;
    wire [7:0] next_mem_164  ;
    wire [7:0] next_mem_165  ;
    wire [7:0] next_mem_166  ;
    wire [7:0] next_mem_167  ;
    wire [7:0] next_mem_168  ;
    wire [7:0] next_mem_169  ;
    wire [7:0] next_mem_170  ;
    wire [7:0] next_mem_171  ;
    wire [7:0] next_mem_172  ;
    wire [7:0] next_mem_173  ;
    wire [7:0] next_mem_174  ;
    wire [7:0] next_mem_175  ;
    wire [7:0] next_mem_176  ;
    wire [7:0] next_mem_177  ;
    wire [7:0] next_mem_178  ;
    wire [7:0] next_mem_179  ;
    wire [7:0] next_mem_180  ;
    wire [7:0] next_mem_181  ;
    wire [7:0] next_mem_182  ;
    wire [7:0] next_mem_183  ;
    wire [7:0] next_mem_184  ;
    wire [7:0] next_mem_185  ;
    wire [7:0] next_mem_186  ;
    wire [7:0] next_mem_187  ;
    wire [7:0] next_mem_188  ;
    wire [7:0] next_mem_189  ;
    wire [7:0] next_mem_190  ;
    wire [7:0] next_mem_191  ;
    wire [7:0] next_mem_192  ;
    wire [7:0] next_mem_193  ;
    wire [7:0] next_mem_194  ;
    wire [7:0] next_mem_195  ;
    wire [7:0] next_mem_196  ;
    wire [7:0] next_mem_197  ;
    wire [7:0] next_mem_198  ;
    wire [7:0] next_mem_199  ;
    wire [7:0] next_mem_200  ;
    wire [7:0] next_mem_201  ;
    wire [7:0] next_mem_202  ;
    wire [7:0] next_mem_203  ;
    wire [7:0] next_mem_204  ;
    wire [7:0] next_mem_205  ;
    wire [7:0] next_mem_206  ;
    wire [7:0] next_mem_207  ;
    wire [7:0] next_mem_208  ;
    wire [7:0] next_mem_209  ;
    wire [7:0] next_mem_210  ;
    wire [7:0] next_mem_211  ;
    wire [7:0] next_mem_212  ;
    wire [7:0] next_mem_213  ;
    wire [7:0] next_mem_214  ;
    wire [7:0] next_mem_215  ;
    wire [7:0] next_mem_216  ;
    wire [7:0] next_mem_217  ;
    wire [7:0] next_mem_218  ;
    wire [7:0] next_mem_219  ;
    wire [7:0] next_mem_220  ;
    wire [7:0] next_mem_221  ;
    wire [7:0] next_mem_222  ;
    wire [7:0] next_mem_223  ;
    wire [7:0] next_mem_224  ;
    wire [7:0] next_mem_225  ;
    wire [7:0] next_mem_226  ;
    wire [7:0] next_mem_227  ;
    wire [7:0] next_mem_228  ;
    wire [7:0] next_mem_229  ;
    wire [7:0] next_mem_230  ;
    wire [7:0] next_mem_231  ;
    wire [7:0] next_mem_232  ;
    wire [7:0] next_mem_233  ;
    wire [7:0] next_mem_234  ;
    wire [7:0] next_mem_235  ;
    wire [7:0] next_mem_236  ;
    wire [7:0] next_mem_237  ;
    wire [7:0] next_mem_238  ;
    wire [7:0] next_mem_239  ;
    wire [7:0] next_mem_240  ;
    wire [7:0] next_mem_241  ;
    wire [7:0] next_mem_242  ;
    wire [7:0] next_mem_243  ;
    wire [7:0] next_mem_244  ;
    wire [7:0] next_mem_245  ;
    wire [7:0] next_mem_246  ;
    wire [7:0] next_mem_247  ;
    wire [7:0] next_mem_248  ;
    wire [7:0] next_mem_249  ;
    wire [7:0] next_mem_250  ;
    wire [7:0] next_mem_251  ;
    wire [7:0] next_mem_252  ;
    wire [7:0] next_mem_253  ;
    wire [7:0] next_mem_254  ;
    wire [7:0] next_mem_255  ;
    
    
    reg [7:0] now_mem_0    ;
    reg [7:0] now_mem_1    ;
    reg [7:0] now_mem_2    ;
    reg [7:0] now_mem_3    ;
    reg [7:0] now_mem_4    ;
    reg [7:0] now_mem_5    ;
    reg [7:0] now_mem_6    ;
    reg [7:0] now_mem_7    ;
    reg [7:0] now_mem_8    ;
    reg [7:0] now_mem_9    ;
    reg [7:0] now_mem_10   ;
    reg [7:0] now_mem_11   ;
    reg [7:0] now_mem_12   ;
    reg [7:0] now_mem_13   ;
    reg [7:0] now_mem_14   ;
    reg [7:0] now_mem_15   ;
    reg [7:0] now_mem_16   ;
    reg [7:0] now_mem_17   ;
    reg [7:0] now_mem_18   ;
    reg [7:0] now_mem_19   ;
    reg [7:0] now_mem_20   ;
    reg [7:0] now_mem_21   ;
    reg [7:0] now_mem_22   ;
    reg [7:0] now_mem_23   ;
    reg [7:0] now_mem_24   ;
    reg [7:0] now_mem_25   ;
    reg [7:0] now_mem_26   ;
    reg [7:0] now_mem_27   ;
    reg [7:0] now_mem_28   ;
    reg [7:0] now_mem_29   ;
    reg [7:0] now_mem_30   ;
    reg [7:0] now_mem_31   ;
    reg [7:0] now_mem_32   ;
    reg [7:0] now_mem_33   ;
    reg [7:0] now_mem_34   ;
    reg [7:0] now_mem_35   ;
    reg [7:0] now_mem_36   ;
    reg [7:0] now_mem_37   ;
    reg [7:0] now_mem_38   ;
    reg [7:0] now_mem_39   ;
    reg [7:0] now_mem_40   ;
    reg [7:0] now_mem_41   ;
    reg [7:0] now_mem_42   ;
    reg [7:0] now_mem_43   ;
    reg [7:0] now_mem_44   ;
    reg [7:0] now_mem_45   ;
    reg [7:0] now_mem_46   ;
    reg [7:0] now_mem_47   ;
    reg [7:0] now_mem_48   ;
    reg [7:0] now_mem_49   ;
    reg [7:0] now_mem_50   ;
    reg [7:0] now_mem_51   ;
    reg [7:0] now_mem_52   ;
    reg [7:0] now_mem_53   ;
    reg [7:0] now_mem_54   ;
    reg [7:0] now_mem_55   ;
    reg [7:0] now_mem_56   ;
    reg [7:0] now_mem_57   ;
    reg [7:0] now_mem_58   ;
    reg [7:0] now_mem_59   ;
    reg [7:0] now_mem_60   ;
    reg [7:0] now_mem_61   ;
    reg [7:0] now_mem_62   ;
    reg [7:0] now_mem_63   ;
    reg [7:0] now_mem_64   ;
    reg [7:0] now_mem_65   ;
    reg [7:0] now_mem_66   ;
    reg [7:0] now_mem_67   ;
    reg [7:0] now_mem_68   ;
    reg [7:0] now_mem_69   ;
    reg [7:0] now_mem_70   ;
    reg [7:0] now_mem_71   ;
    reg [7:0] now_mem_72   ;
    reg [7:0] now_mem_73   ;
    reg [7:0] now_mem_74   ;
    reg [7:0] now_mem_75   ;
    reg [7:0] now_mem_76   ;
    reg [7:0] now_mem_77   ;
    reg [7:0] now_mem_78   ;
    reg [7:0] now_mem_79   ;
    reg [7:0] now_mem_80   ;
    reg [7:0] now_mem_81   ;
    reg [7:0] now_mem_82   ;
    reg [7:0] now_mem_83   ;
    reg [7:0] now_mem_84   ;
    reg [7:0] now_mem_85   ;
    reg [7:0] now_mem_86   ;
    reg [7:0] now_mem_87   ;
    reg [7:0] now_mem_88   ;
    reg [7:0] now_mem_89   ;
    reg [7:0] now_mem_90   ;
    reg [7:0] now_mem_91   ;
    reg [7:0] now_mem_92   ;
    reg [7:0] now_mem_93   ;
    reg [7:0] now_mem_94   ;
    reg [7:0] now_mem_95   ;
    reg [7:0] now_mem_96   ;
    reg [7:0] now_mem_97   ;
    reg [7:0] now_mem_98   ;
    reg [7:0] now_mem_99   ;
    reg [7:0] now_mem_100  ;
    reg [7:0] now_mem_101  ;
    reg [7:0] now_mem_102  ;
    reg [7:0] now_mem_103  ;
    reg [7:0] now_mem_104  ;
    reg [7:0] now_mem_105  ;
    reg [7:0] now_mem_106  ;
    reg [7:0] now_mem_107  ;
    reg [7:0] now_mem_108  ;
    reg [7:0] now_mem_109  ;
    reg [7:0] now_mem_110  ;
    reg [7:0] now_mem_111  ;
    reg [7:0] now_mem_112  ;
    reg [7:0] now_mem_113  ;
    reg [7:0] now_mem_114  ;
    reg [7:0] now_mem_115  ;
    reg [7:0] now_mem_116  ;
    reg [7:0] now_mem_117  ;
    reg [7:0] now_mem_118  ;
    reg [7:0] now_mem_119  ;
    reg [7:0] now_mem_120  ;
    reg [7:0] now_mem_121  ;
    reg [7:0] now_mem_122  ;
    reg [7:0] now_mem_123  ;
    reg [7:0] now_mem_124  ;
    reg [7:0] now_mem_125  ;
    reg [7:0] now_mem_126  ;
    reg [7:0] now_mem_127  ;
    reg [7:0] now_mem_128  ;
    reg [7:0] now_mem_129  ;
    reg [7:0] now_mem_130  ;
    reg [7:0] now_mem_131  ;
    reg [7:0] now_mem_132  ;
    reg [7:0] now_mem_133  ;
    reg [7:0] now_mem_134  ;
    reg [7:0] now_mem_135  ;
    reg [7:0] now_mem_136  ;
    reg [7:0] now_mem_137  ;
    // reg [7:0] now_mem_138  ;
    // reg [7:0] now_mem_139  ;
    // reg [7:0] now_mem_140  ;
    // reg [7:0] now_mem_141  ;
    reg [7:0] now_mem_142  ;
    reg [7:0] now_mem_143  ;
    reg [7:0] now_mem_144  ;
    reg [7:0] now_mem_145  ;
    reg [7:0] now_mem_146  ;
    reg [7:0] now_mem_147  ;
    reg [7:0] now_mem_148  ;
    reg [7:0] now_mem_149  ;
    reg [7:0] now_mem_150  ;
    reg [7:0] now_mem_151  ;
    reg [7:0] now_mem_152  ;
    reg [7:0] now_mem_153  ;
    reg [7:0] now_mem_154  ;
    reg [7:0] now_mem_155  ;
    reg [7:0] now_mem_156  ;
    reg [7:0] now_mem_157  ;
    reg [7:0] now_mem_158  ;
    reg [7:0] now_mem_159  ;
    reg [7:0] now_mem_160  ;
    reg [7:0] now_mem_161  ;
    reg [7:0] now_mem_162  ;
    reg [7:0] now_mem_163  ;
    reg [7:0] now_mem_164  ;
    reg [7:0] now_mem_165  ;
    reg [7:0] now_mem_166  ;
    reg [7:0] now_mem_167  ;
    reg [7:0] now_mem_168  ;
    reg [7:0] now_mem_169  ;
    reg [7:0] now_mem_170  ;
    reg [7:0] now_mem_171  ;
    reg [7:0] now_mem_172  ;
    reg [7:0] now_mem_173  ;
    reg [7:0] now_mem_174  ;
    reg [7:0] now_mem_175  ;
    reg [7:0] now_mem_176  ;
    reg [7:0] now_mem_177  ;
    reg [7:0] now_mem_178  ;
    reg [7:0] now_mem_179  ;
    reg [7:0] now_mem_180  ;
    reg [7:0] now_mem_181  ;
    reg [7:0] now_mem_182  ;
    reg [7:0] now_mem_183  ;
    reg [7:0] now_mem_184  ;
    reg [7:0] now_mem_185  ;
    reg [7:0] now_mem_186  ;
    reg [7:0] now_mem_187  ;
    reg [7:0] now_mem_188  ;
    reg [7:0] now_mem_189  ;
    reg [7:0] now_mem_190  ;
    reg [7:0] now_mem_191  ;
    reg [7:0] now_mem_192  ;
    reg [7:0] now_mem_193  ;
    reg [7:0] now_mem_194  ;
    reg [7:0] now_mem_195  ;
    reg [7:0] now_mem_196  ;
    reg [7:0] now_mem_197  ;
    reg [7:0] now_mem_198  ;
    reg [7:0] now_mem_199  ;
    reg [7:0] now_mem_200  ;
    reg [7:0] now_mem_201  ;
    reg [7:0] now_mem_202  ;
    reg [7:0] now_mem_203  ;
    reg [7:0] now_mem_204  ;
    reg [7:0] now_mem_205  ;
    reg [7:0] now_mem_206  ;
    reg [7:0] now_mem_207  ;
    reg [7:0] now_mem_208  ;
    reg [7:0] now_mem_209  ;
    reg [7:0] now_mem_210  ;
    reg [7:0] now_mem_211  ;
    reg [7:0] now_mem_212  ;
    reg [7:0] now_mem_213  ;
    reg [7:0] now_mem_214  ;
    reg [7:0] now_mem_215  ;
    reg [7:0] now_mem_216  ;
    reg [7:0] now_mem_217  ;
    reg [7:0] now_mem_218  ;
    reg [7:0] now_mem_219  ;
    reg [7:0] now_mem_220  ;
    reg [7:0] now_mem_221  ;
    reg [7:0] now_mem_222  ;
    reg [7:0] now_mem_223  ;
    reg [7:0] now_mem_224  ;
    reg [7:0] now_mem_225  ;
    reg [7:0] now_mem_226  ;
    reg [7:0] now_mem_227  ;
    reg [7:0] now_mem_228  ;
    reg [7:0] now_mem_229  ;
    reg [7:0] now_mem_230  ;
    reg [7:0] now_mem_231  ;
    reg [7:0] now_mem_232  ;
    reg [7:0] now_mem_233  ;
    reg [7:0] now_mem_234  ;
    reg [7:0] now_mem_235  ;
    reg [7:0] now_mem_236  ;
    reg [7:0] now_mem_237  ;
    reg [7:0] now_mem_238  ;
    reg [7:0] now_mem_239  ;
    reg [7:0] now_mem_240  ;
    reg [7:0] now_mem_241  ;
    reg [7:0] now_mem_242  ;
    reg [7:0] now_mem_243  ;
    reg [7:0] now_mem_244  ;
    reg [7:0] now_mem_245  ;
    reg [7:0] now_mem_246  ;
    reg [7:0] now_mem_247  ;
    reg [7:0] now_mem_248  ;
    reg [7:0] now_mem_249  ;
    reg [7:0] now_mem_250  ;
    reg [7:0] now_mem_251  ;
    reg [7:0] now_mem_252  ;
    reg [7:0] now_mem_253  ;
    reg [7:0] now_mem_254  ;
    reg [7:0] now_mem_255  ;
    
    wire [7:0] now_mem_138  ;
    wire [7:0] now_mem_139  ;
    wire [7:0] now_mem_140  ;
    wire [7:0] now_mem_141  ;
    
	 wire TCON_5_wire;
	 wire TCON_7_wire;
     
     wire TMR0_H_wen = mem_write_en_in?(w_addr == TH0_ADDR):0;
     wire TMR0_L_wen = mem_write_en_in?(w_addr == TL0_ADDR):0;
     wire TMR1_H_wen = mem_write_en_in?(w_addr == TH1_ADDR):0;
     wire TMR1_L_wen = mem_write_en_in?(w_addr == TL1_ADDR):0;
     
	 
	 wire TMR0_wen, TMR1_wen;
	 
	 reg [7:0] timer_in;
     
	 always@(*) begin
		if(w_addr == TL0_ADDR && mem_write_en_in) begin
			timer_in = next_mem_138;
		end
		else if(w_addr == TL1_ADDR && mem_write_en_in) begin
			timer_in = next_mem_139;
		end
		else if(w_addr == TH0_ADDR && mem_write_en_in) begin
			timer_in = next_mem_140;
		end
		else if(w_addr == TH1_ADDR && mem_write_en_in) begin
			timer_in = next_mem_141;
		end
        else begin
            timer_in = 8'd0;
        end
	 end     

	 timer_top timer_module (
		.clk(clk), 
		.reset(reset), 
		.TMOD_in(`SFR_TMOD), 
		.TCON_in(`SFR_TCON), 
		.TCON_5_out(TCON_5_wire), 
		.TCON_7_out(TCON_7_wire), 
		.pin_T0(port3_in[4]), 
		.pin_T1(port3_in[5]), 
		.pin_INT0(1'b0), 
		.pin_INT1(1'b0), 
		.TMR_in(timer_in), 
		.TMR0_out({now_mem_140,now_mem_138}), 
		.TMR1_out({now_mem_141,now_mem_139}), 
		.TMR0_H_wen(TMR0_H_wen), 
		.TMR0_L_wen(w_addr == TL0_ADDR), 
		.TMR1_H_wen(TMR0_L_wen), 
		.TMR1_L_wen(TMR1_L_wen)
    );
// Instantiate the module
     
    
    assign dptr_out = {next_mem_131,next_mem_130};
    
   
    assign psw_out = {now_mem_208[7:6],now_mem_208[2]};
    
    assign  serial_ren_out = `SFR_SCON[4];
    assign  serial_enable_out = `SFR_TCON[6];
    assign txdata_serial_out = `SFR_SBUF;
    assign serial_ti_flag_out = `SFR_SCON[1];
    assign serial_ri_flag_out = `SFR_SCON[0];
  
    //---------------------------------------------------------------------------------------------------------------------
    // Implmentation
    //---------------------------------------------------------------------------------------------------------------------
    assign port0_out = `SFR_P0;
    assign port1_out = `SFR_P1;
    assign port2_out = `SFR_P2;
    assign port3_out = `SFR_P3;
    
    assign r0b0_r = next_mem_0;
    assign r0b1_r = next_mem_8;
    assign r0b2_r = next_mem_16;
    assign r0b3_r = next_mem_24;
    assign r1b0_r = next_mem_1;
    assign r1b1_r = next_mem_9;
    assign r1b2_r = next_mem_17;
    assign r1b3_r = next_mem_25;
    
    assign r0b0_w = now_mem_0;
    assign r0b1_w = now_mem_8;
    assign r0b2_w = now_mem_16;
    assign r0b3_w = now_mem_24;
    assign r1b0_w = now_mem_1;
    assign r1b1_w = now_mem_9;
    assign r1b2_w = now_mem_17;
    assign r1b3_w = now_mem_25;
    
    assign b_sel_w = now_mem_208[4:3];
    
    assign b_sel_r = next_mem_208[4:3];
    
    //assign r_addr0_d = ACC_ADDR;
    
    assign acc_out = next_mem_224;
    
    assign parity_bit = ^(next_mem_224);
    
    //assign accu_input = (w_addr == ACC_ADDR)? w_data:mem[ACC_ADDR];
    
    
    always@(*) begin
        case(b_sel_r) 
            B_SEL_0: begin
                r0_mux_in_r = r0b0_r;
                r1_mux_in_r = r1b0_r;
            end
            B_SEL_1: begin
                r0_mux_in_r = r0b1_r;
                r1_mux_in_r = r1b1_r;              
            end 
            B_SEL_2: begin
                r0_mux_in_r = r0b2_r;
                r1_mux_in_r = r1b2_r;              
            end
            B_SEL_3: begin
                r0_mux_in_r = r0b3_r;
                r1_mux_in_r = r1b3_r;              
            end
            default: begin
                r0_mux_in_r = 8'hxx;
                r1_mux_in_r = 8'hxx;
            
            end
            
        endcase
    end
 
    always@(*) begin
        case(b_sel_w) 
            B_SEL_0: begin
                r0_mux_in_w = r0b0_w;
                r1_mux_in_w = r1b0_w;
                // R0_addr = 8'h00;
                // R1_addr = 8'h01;
            end
            B_SEL_1: begin
                r0_mux_in_w = r0b1_w;
                r1_mux_in_w = r1b1_w;
                // R0_addr = 8'h08;
                // R1_addr = 8'h09;                
            end 
            B_SEL_2: begin
                r0_mux_in_w = r0b2_w;
                r1_mux_in_w = r1b2_w;
                // R0_addr = 8'h10;
                // R1_addr = 8'h11;                
            end
            B_SEL_3: begin
                r0_mux_in_w = r0b3_w;
                r1_mux_in_w = r1b3_w;
                // R0_addr = 8'h18;
                // R1_addr = 8'h19;               
            end
            default: begin
                r0_mux_in_w = 8'hxx;
                r1_mux_in_w = 8'hxx;
                // R0_addr = 0;
                // R1_addr = 0;
            
            end
            
        endcase
    end

    
    always@* begin
        case(mr_addr1_sel_in) 
            `RADD_R0: begin
                rd_mx_out1 = r0_mux_in_r;
            end
            `RADD_R1: begin
                rd_mx_out1 = r1_mux_in_r;
            end
            `RADD_OP1: begin
                rd_mx_out1 = op1_1d_in;
            end
            `RADD_Rn: begin
                rd_mx_out1 = {3'b000,b_sel_r[1:0], op_1d_3LSB_in};
            end
            `RADD_ACC: begin
                rd_mx_out1 = ACC_ADDR;
            end
            `RADD_OP2: begin
                rd_mx_out1 = op2_1d_in;
            end            
            `RADD_SP: begin
                rd_mx_out1 = next_mem_129;
            end
            `RADD_DPTRL: begin
                rd_mx_out1 = DPL_ADDR;
            end
            `RADD_DPTRH: begin
                rd_mx_out1 = DPH_ADDR;
            end
            `RADD_B: begin
                rd_mx_out1 = B_ADDR;
            end
            default: begin
                rd_mx_out1 = 8'hxx;
            end
        endcase
    end
    


    always@* begin
        case({mr_addr1_bit_in,rd_mx_out1[7]}) 
            MR_AD1_BIT_L: begin
                r_addr1 = 8'h20 + rd_mx_out1[3:0];
            end
            MR_AD1_BIT_H: begin
                r_addr1 = {1'b1,rd_mx_out1[6:3],3'b000};
            end
            default:begin
                r_addr1 = rd_mx_out1;
            end
        endcase
    end	
   
	always@* begin
        case(mem_w_addr_sel_in)
            `WADD_SP1: begin
                wr_mx_out = (now_mem_129 + 1)%256;
            end
            `WADD_ACC: begin
                wr_mx_out = ACC_ADDR;
            end
            `WADD_OP1: begin
                wr_mx_out = op1_2d_in;
            end
            `WADD_OP2: begin
                wr_mx_out = op2_2d_in;
            end
            `WADD_Rn: begin
                wr_mx_out = {3'b000,b_sel_w[1:0], op_2d_3LSB_in};
            end
            `WADD_DPTRL: begin
                wr_mx_out = DPL_ADDR;
            end
            `WADD_DPTRH: begin
                wr_mx_out = DPH_ADDR;
            end
            `WADD_R0: begin
                wr_mx_out = r0_mux_in_w;
            end
            `WADD_R1: begin
                wr_mx_out = r1_mux_in_w;
            end
            `WADD_B: begin
                wr_mx_out = B_ADDR;
            end
            default: begin
                wr_mx_out = 8'h00;
            end
        endcase
    end
    
    always@* begin
        case({mem_write_bit_in,wr_mx_out[7]}) 
            MR_AD1_BIT_L: begin
                w_addr = 8'h20 + wr_mx_out[3:0];
            end
            MR_AD1_BIT_H: begin
                w_addr = {1'b1,wr_mx_out[6:3],3'b000};
            end
            default:begin
                w_addr = wr_mx_out;
            end
        endcase
    end	

    always@* begin
        case(mem_write_sel_in)
            MW_DT_ALU: begin
                w_data = mw_data_in;
            end
            MW_DT_PM: begin
                w_data = pm_data_in;
            end
            default: begin
                w_data = 8'hxx;
            end
        endcase
    end
    
    always@(posedge clk) begin
        r_addr1_d <= r_addr1;
    end

    assign next_mem_0   = ((w_addr == 8'd0  )&(mem_write_en_in == 1'b1))?   w_data: now_mem_0  ;
    assign next_mem_1   = ((w_addr == 8'd1  ))?   w_data: now_mem_1  ;
    assign next_mem_2   = ((w_addr == 8'd2  ))?   w_data: now_mem_2  ;
    assign next_mem_3   = ((w_addr == 8'd3  ))?   w_data: now_mem_3  ;
    assign next_mem_4   = ((w_addr == 8'd4  ))?   w_data: now_mem_4  ;
    assign next_mem_5   = ((w_addr == 8'd5  ))?   w_data: now_mem_5  ;
    assign next_mem_6   = ((w_addr == 8'd6  ))?   w_data: now_mem_6  ;
    assign next_mem_7   = ((w_addr == 8'd7  ))?   w_data: now_mem_7  ;
    assign next_mem_8   = ((w_addr == 8'd8  ))?   w_data: now_mem_8  ;
    assign next_mem_9   = ((w_addr == 8'd9  ))?   w_data: now_mem_9  ;
    assign next_mem_10  = ((w_addr == 8'd10 ))?   w_data: now_mem_10 ;
    assign next_mem_11  = ((w_addr == 8'd11 ))?   w_data: now_mem_11 ;
    assign next_mem_12  = ((w_addr == 8'd12 ))?   w_data: now_mem_12 ;
    assign next_mem_13  = ((w_addr == 8'd13 ))?   w_data: now_mem_13 ;
    assign next_mem_14  = ((w_addr == 8'd14 ))?   w_data: now_mem_14 ;
    assign next_mem_15  = ((w_addr == 8'd15 ))?   w_data: now_mem_15 ;
    assign next_mem_16  = ((w_addr == 8'd16 ))?   w_data: now_mem_16 ;
    assign next_mem_17  = ((w_addr == 8'd17 ))?   w_data: now_mem_17 ;
    assign next_mem_18  = ((w_addr == 8'd18 ))?   w_data: now_mem_18 ;
    assign next_mem_19  = ((w_addr == 8'd19 ))?   w_data: now_mem_19 ;
    assign next_mem_20  = ((w_addr == 8'd20 ))?   w_data: now_mem_20 ;
    assign next_mem_21  = ((w_addr == 8'd21 ))?   w_data: now_mem_21 ;
    assign next_mem_22  = ((w_addr == 8'd22 ))?   w_data: now_mem_22 ;
    assign next_mem_23  = ((w_addr == 8'd23 ))?   w_data: now_mem_23 ;
    assign next_mem_24  = ((w_addr == 8'd24 ))?   w_data: now_mem_24 ;
    assign next_mem_25  = ((w_addr == 8'd25 ))?   w_data: now_mem_25 ;
    assign next_mem_26  = ((w_addr == 8'd26 ))?   w_data: now_mem_26 ;
    assign next_mem_27  = ((w_addr == 8'd27 ))?   w_data: now_mem_27 ;
    assign next_mem_28  = ((w_addr == 8'd28 ))?   w_data: now_mem_28 ;
    assign next_mem_29  = ((w_addr == 8'd29 ))?   w_data: now_mem_29 ;
    assign next_mem_30  = ((w_addr == 8'd30 ))?   w_data: now_mem_30 ;
    assign next_mem_31  = ((w_addr == 8'd31 ))?   w_data: now_mem_31 ;
    assign next_mem_32  = ((w_addr == 8'd32 ))?   w_data: now_mem_32 ;
    assign next_mem_33  = ((w_addr == 8'd33 ))?   w_data: now_mem_33 ;
    assign next_mem_34  = ((w_addr == 8'd34 ))?   w_data: now_mem_34 ;
    assign next_mem_35  = ((w_addr == 8'd35 ))?   w_data: now_mem_35 ;
    assign next_mem_36  = ((w_addr == 8'd36 ))?   w_data: now_mem_36 ;
    assign next_mem_37  = ((w_addr == 8'd37 ))?   w_data: now_mem_37 ;
    assign next_mem_38  = ((w_addr == 8'd38 ))?   w_data: now_mem_38 ;
    assign next_mem_39  = ((w_addr == 8'd39 ))?   w_data: now_mem_39 ;
    assign next_mem_40  = ((w_addr == 8'd40 ))?   w_data: now_mem_40 ;
    assign next_mem_41  = ((w_addr == 8'd41 ))?   w_data: now_mem_41 ;
    assign next_mem_42  = ((w_addr == 8'd42 ))?   w_data: now_mem_42 ;
    assign next_mem_43  = ((w_addr == 8'd43 ))?   w_data: now_mem_43 ;
    assign next_mem_44  = ((w_addr == 8'd44 ))?   w_data: now_mem_44 ;
    assign next_mem_45  = ((w_addr == 8'd45 ))?   w_data: now_mem_45 ;
    assign next_mem_46  = ((w_addr == 8'd46 ))?   w_data: now_mem_46 ;
    assign next_mem_47  = ((w_addr == 8'd47 ))?   w_data: now_mem_47 ;
    assign next_mem_48  = ((w_addr == 8'd48 ))?   w_data: now_mem_48 ;
    assign next_mem_49  = ((w_addr == 8'd49 ))?   w_data: now_mem_49 ;
    assign next_mem_50  = ((w_addr == 8'd50 ))?   w_data: now_mem_50 ;
    assign next_mem_51  = ((w_addr == 8'd51 ))?   w_data: now_mem_51 ;
    assign next_mem_52  = ((w_addr == 8'd52 ))?   w_data: now_mem_52 ;
    assign next_mem_53  = ((w_addr == 8'd53 ))?   w_data: now_mem_53 ;
    assign next_mem_54  = ((w_addr == 8'd54 ))?   w_data: now_mem_54 ;
    assign next_mem_55  = ((w_addr == 8'd55 ))?   w_data: now_mem_55 ;
    assign next_mem_56  = ((w_addr == 8'd56 ))?   w_data: now_mem_56 ;
    assign next_mem_57  = ((w_addr == 8'd57 ))?   w_data: now_mem_57 ;
    assign next_mem_58  = ((w_addr == 8'd58 ))?   w_data: now_mem_58 ;
    assign next_mem_59  = ((w_addr == 8'd59 ))?   w_data: now_mem_59 ;
    assign next_mem_60  = ((w_addr == 8'd60 ))?   w_data: now_mem_60 ;
    assign next_mem_61  = ((w_addr == 8'd61 ))?   w_data: now_mem_61 ;
    assign next_mem_62  = ((w_addr == 8'd62 ))?   w_data: now_mem_62 ;
    assign next_mem_63  = ((w_addr == 8'd63 ))?   w_data: now_mem_63 ;
    assign next_mem_64  = ((w_addr == 8'd64 ))?   w_data: now_mem_64 ;
    assign next_mem_65  = ((w_addr == 8'd65 ))?   w_data: now_mem_65 ;
    assign next_mem_66  = ((w_addr == 8'd66 ))?   w_data: now_mem_66 ;
    assign next_mem_67  = ((w_addr == 8'd67 ))?   w_data: now_mem_67 ;
    assign next_mem_68  = ((w_addr == 8'd68 ))?   w_data: now_mem_68 ;
    assign next_mem_69  = ((w_addr == 8'd69 ))?   w_data: now_mem_69 ;
    assign next_mem_70  = ((w_addr == 8'd70 ))?   w_data: now_mem_70 ;
    assign next_mem_71  = ((w_addr == 8'd71 ))?   w_data: now_mem_71 ;
    assign next_mem_72  = ((w_addr == 8'd72 ))?   w_data: now_mem_72 ;
    assign next_mem_73  = ((w_addr == 8'd73 ))?   w_data: now_mem_73 ;
    assign next_mem_74  = ((w_addr == 8'd74 ))?   w_data: now_mem_74 ;
    assign next_mem_75  = ((w_addr == 8'd75 ))?   w_data: now_mem_75 ;
    assign next_mem_76  = ((w_addr == 8'd76 ))?   w_data: now_mem_76 ;
    assign next_mem_77  = ((w_addr == 8'd77 ))?   w_data: now_mem_77 ;
    assign next_mem_78  = ((w_addr == 8'd78 ))?   w_data: now_mem_78 ;
    assign next_mem_79  = ((w_addr == 8'd79 ))?   w_data: now_mem_79 ;
    assign next_mem_80  = ((w_addr == 8'd80 ))?   w_data: now_mem_80 ;
    assign next_mem_81  = ((w_addr == 8'd81 ))?   w_data: now_mem_81 ;
    assign next_mem_82  = ((w_addr == 8'd82 ))?   w_data: now_mem_82 ;
    assign next_mem_83  = ((w_addr == 8'd83 ))?   w_data: now_mem_83 ;
    assign next_mem_84  = ((w_addr == 8'd84 ))?   w_data: now_mem_84 ;
    assign next_mem_85  = ((w_addr == 8'd85 ))?   w_data: now_mem_85 ;
    assign next_mem_86  = ((w_addr == 8'd86 ))?   w_data: now_mem_86 ;
    assign next_mem_87  = ((w_addr == 8'd87 ))?   w_data: now_mem_87 ;
    assign next_mem_88  = ((w_addr == 8'd88 ))?   w_data: now_mem_88 ;
    assign next_mem_89  = ((w_addr == 8'd89 ))?   w_data: now_mem_89 ;
    assign next_mem_90  = ((w_addr == 8'd90 ))?   w_data: now_mem_90 ;
    assign next_mem_91  = ((w_addr == 8'd91 ))?   w_data: now_mem_91 ;
    assign next_mem_92  = ((w_addr == 8'd92 ))?   w_data: now_mem_92 ;
    assign next_mem_93  = ((w_addr == 8'd93 ))?   w_data: now_mem_93 ;
    assign next_mem_94  = ((w_addr == 8'd94 ))?   w_data: now_mem_94 ;
    assign next_mem_95  = ((w_addr == 8'd95 ))?   w_data: now_mem_95 ;
    assign next_mem_96  = ((w_addr == 8'd96 ))?   w_data: now_mem_96 ;
    assign next_mem_97  = ((w_addr == 8'd97 ))?   w_data: now_mem_97 ;
    assign next_mem_98  = ((w_addr == 8'd98 ))?   w_data: now_mem_98 ;
    assign next_mem_99  = ((w_addr == 8'd99 ))?   w_data: now_mem_99 ;
    assign next_mem_100 = ((w_addr == 8'd100))?   w_data: now_mem_100;
    assign next_mem_101 = ((w_addr == 8'd101))?   w_data: now_mem_101;
    assign next_mem_102 = ((w_addr == 8'd102))?   w_data: now_mem_102;
    assign next_mem_103 = ((w_addr == 8'd103))?   w_data: now_mem_103;
    assign next_mem_104 = ((w_addr == 8'd104))?   w_data: now_mem_104;
    assign next_mem_105 = ((w_addr == 8'd105))?   w_data: now_mem_105;
    assign next_mem_106 = ((w_addr == 8'd106))?   w_data: now_mem_106;
    assign next_mem_107 = ((w_addr == 8'd107))?   w_data: now_mem_107;
    assign next_mem_108 = ((w_addr == 8'd108))?   w_data: now_mem_108;
    assign next_mem_109 = ((w_addr == 8'd109))?   w_data: now_mem_109;
    assign next_mem_110 = ((w_addr == 8'd110))?   w_data: now_mem_110;
    assign next_mem_111 = ((w_addr == 8'd111))?   w_data: now_mem_111;
    assign next_mem_112 = ((w_addr == 8'd112))?   w_data: now_mem_112;
    assign next_mem_113 = ((w_addr == 8'd113))?   w_data: now_mem_113;
    assign next_mem_114 = ((w_addr == 8'd114))?   w_data: now_mem_114;
    assign next_mem_115 = ((w_addr == 8'd115))?   w_data: now_mem_115;
    assign next_mem_116 = ((w_addr == 8'd116))?   w_data: now_mem_116;
    assign next_mem_117 = ((w_addr == 8'd117))?   w_data: now_mem_117;
    assign next_mem_118 = ((w_addr == 8'd118))?   w_data: now_mem_118;
    assign next_mem_119 = ((w_addr == 8'd119))?   w_data: now_mem_119;
    assign next_mem_120 = ((w_addr == 8'd120))?   w_data: now_mem_120;
    assign next_mem_121 = ((w_addr == 8'd121))?   w_data: now_mem_121;
    assign next_mem_122 = ((w_addr == 8'd122))?   w_data: now_mem_122;
    assign next_mem_123 = ((w_addr == 8'd123))?   w_data: now_mem_123;
    assign next_mem_124 = ((w_addr == 8'd124))?   w_data: now_mem_124;
    assign next_mem_125 = ((w_addr == 8'd125))?   w_data: now_mem_125;
    assign next_mem_126 = ((w_addr == 8'd126))?   w_data: now_mem_126;
    assign next_mem_127 = ((w_addr == 8'd127))?   w_data: now_mem_127;
    assign next_mem_128 = ((w_addr == 8'd128))?   w_data: now_mem_128;
    always@* begin
        if(w_addr == SP_ADDR && sp_dec_in) begin
            `SFR_SP = (w_data - 1) % 256;
        end
        else if(w_addr == 8'd129) begin 
            next_mem_129 = w_data;
        end
        else if(sp_dec_in) begin
            next_mem_129 = (now_mem_129 - 1)%256;
        end
        else if((mem_w_addr_sel_in == `WADD_SP1)) begin
            next_mem_129 = (now_mem_129 + 1)%256;
        end
        else begin
            next_mem_129 = now_mem_129;
        end
    end
    // assign next_mem_129 = (w_addr == 8'd129)?   w_data: 
                         // ((sp_dec_in && (w_addr != SP_ADDR))?((now_mem_129 - 1)%256):
                         // (((mem_w_addr_sel_in == `WADD_SP1) && (w_addr != SP_ADDR))? ((now_mem_129 + 1)%256):now_mem_129));
    assign next_mem_130 = ((w_addr == 8'd130))?   w_data: now_mem_130;
    assign next_mem_131 = ((w_addr == 8'd131))?   w_data: now_mem_131;
    assign next_mem_132 = ((w_addr == 8'd132))?   w_data: now_mem_132;
    assign next_mem_133 = ((w_addr == 8'd133))?   w_data: now_mem_133;
    assign next_mem_134 = ((w_addr == 8'd134))?   w_data: now_mem_134;
    assign next_mem_135 = ((w_addr == 8'd135))?   w_data: now_mem_135;
    assign next_mem_136 = ((w_addr == 8'd136))?   w_data: {TCON_7_wire,now_mem_136[6],TCON_5_wire,now_mem_136[4:0]};
    assign next_mem_137 = ((w_addr == 8'd137))?   w_data: now_mem_137;
    assign next_mem_138 = ((w_addr == 8'd138))?   w_data: now_mem_138;
    assign next_mem_139 = ((w_addr == 8'd139))?   w_data: now_mem_139;
    assign next_mem_140 = ((w_addr == 8'd140))?   w_data: now_mem_140;
    assign next_mem_141 = ((w_addr == 8'd141))?   w_data: now_mem_141;
    assign next_mem_142 = ((w_addr == 8'd142))?   w_data: now_mem_142;
    assign next_mem_143 = ((w_addr == 8'd143))?   w_data: now_mem_143;
    assign next_mem_144 = ((w_addr == 8'd144))?   w_data: now_mem_144;
    assign next_mem_145 = ((w_addr == 8'd145))?   w_data: now_mem_145;
    assign next_mem_146 = ((w_addr == 8'd146))?   w_data: now_mem_146;
    assign next_mem_147 = ((w_addr == 8'd147))?   w_data: now_mem_147;
    assign next_mem_148 = ((w_addr == 8'd148))?   w_data: now_mem_148;
    assign next_mem_149 = ((w_addr == 8'd149))?   w_data: now_mem_149;
    assign next_mem_150 = ((w_addr == 8'd150))?   w_data: now_mem_150;
    assign next_mem_151 = ((w_addr == 8'd151))?   w_data: now_mem_151;
    assign next_mem_152 = ((w_addr == 8'd152))?   w_data: ((serial_rx_done_in)?{now_mem_152[7:1],1'b1}:((serial_tx_done_in)?({now_mem_152[7:2],1'b1,now_mem_152[0]}):now_mem_152));
    assign next_mem_153 = ((w_addr == 8'd153))?   w_data: ((serial_rx_done_in)?rxdata_serial_in:now_mem_153);
    assign next_mem_154 = ((w_addr == 8'd154))?   w_data: now_mem_154;
    assign next_mem_155 = ((w_addr == 8'd155))?   w_data: now_mem_155;
    assign next_mem_156 = ((w_addr == 8'd156))?   w_data: now_mem_156;
    assign next_mem_157 = ((w_addr == 8'd157))?   w_data: now_mem_157;
    assign next_mem_158 = ((w_addr == 8'd158))?   w_data: now_mem_158;
    assign next_mem_159 = ((w_addr == 8'd159))?   w_data: now_mem_159;
    assign next_mem_160 = ((w_addr == 8'd160))?   w_data: now_mem_160;
    assign next_mem_161 = ((w_addr == 8'd161))?   w_data: now_mem_161;
    assign next_mem_162 = ((w_addr == 8'd162))?   w_data: now_mem_162;
    assign next_mem_163 = ((w_addr == 8'd163))?   w_data: now_mem_163;
    assign next_mem_164 = ((w_addr == 8'd164))?   w_data: now_mem_164;
    assign next_mem_165 = ((w_addr == 8'd165))?   w_data: now_mem_165;
    assign next_mem_166 = ((w_addr == 8'd166))?   w_data: now_mem_166;
    assign next_mem_167 = ((w_addr == 8'd167))?   w_data: now_mem_167;
    assign next_mem_168 = ((w_addr == 8'd168))?   w_data: now_mem_168;
    assign next_mem_169 = ((w_addr == 8'd169))?   w_data: now_mem_169;
    assign next_mem_170 = ((w_addr == 8'd170))?   w_data: now_mem_170;
    assign next_mem_171 = ((w_addr == 8'd171))?   w_data: now_mem_171;
    assign next_mem_172 = ((w_addr == 8'd172))?   w_data: now_mem_172;
    assign next_mem_173 = ((w_addr == 8'd173))?   w_data: now_mem_173;
    assign next_mem_174 = ((w_addr == 8'd174))?   w_data: now_mem_174;
    assign next_mem_175 = ((w_addr == 8'd175))?   w_data: now_mem_175;
    assign next_mem_176 = ((w_addr == 8'd176))?   w_data: now_mem_176;
    assign next_mem_177 = ((w_addr == 8'd177))?   w_data: now_mem_177;
    assign next_mem_178 = ((w_addr == 8'd178))?   w_data: now_mem_178;
    assign next_mem_179 = ((w_addr == 8'd179))?   w_data: now_mem_179;
    assign next_mem_180 = ((w_addr == 8'd180))?   w_data: now_mem_180;
    assign next_mem_181 = ((w_addr == 8'd181))?   w_data: now_mem_181;
    assign next_mem_182 = ((w_addr == 8'd182))?   w_data: now_mem_182;
    assign next_mem_183 = ((w_addr == 8'd183))?   w_data: now_mem_183;
    assign next_mem_184 = ((w_addr == 8'd184))?   w_data: now_mem_184;
    assign next_mem_185 = ((w_addr == 8'd185))?   w_data: now_mem_185;
    assign next_mem_186 = ((w_addr == 8'd186))?   w_data: now_mem_186;
    assign next_mem_187 = ((w_addr == 8'd187))?   w_data: now_mem_187;
    assign next_mem_188 = ((w_addr == 8'd188))?   w_data: now_mem_188;
    assign next_mem_189 = ((w_addr == 8'd189))?   w_data: now_mem_189;
    assign next_mem_190 = ((w_addr == 8'd190))?   w_data: now_mem_190;
    assign next_mem_191 = ((w_addr == 8'd191))?   w_data: now_mem_191;
    assign next_mem_192 = ((w_addr == 8'd192))?   w_data: now_mem_192;
    assign next_mem_193 = ((w_addr == 8'd193))?   w_data: now_mem_193;
    assign next_mem_194 = ((w_addr == 8'd194))?   w_data: now_mem_194;
    assign next_mem_195 = ((w_addr == 8'd195))?   w_data: now_mem_195;
    assign next_mem_196 = ((w_addr == 8'd196))?   w_data: now_mem_196;
    assign next_mem_197 = ((w_addr == 8'd197))?   w_data: now_mem_197;
    assign next_mem_198 = ((w_addr == 8'd198))?   w_data: now_mem_198;
    assign next_mem_199 = ((w_addr == 8'd199))?   w_data: now_mem_199;
    assign next_mem_200 = ((w_addr == 8'd200))?   w_data: now_mem_200;
    assign next_mem_201 = ((w_addr == 8'd201))?   w_data: now_mem_201;
    assign next_mem_202 = ((w_addr == 8'd202))?   w_data: now_mem_202;
    assign next_mem_203 = ((w_addr == 8'd203))?   w_data: now_mem_203;
    assign next_mem_204 = ((w_addr == 8'd204))?   w_data: now_mem_204;
    assign next_mem_205 = ((w_addr == 8'd205))?   w_data: now_mem_205;
    assign next_mem_206 = ((w_addr == 8'd206))?   w_data: now_mem_206;
    assign next_mem_207 = ((w_addr == 8'd207))?   w_data: now_mem_207;
    assign next_mem_208 = ((w_addr == 8'd208))?   {w_data[7:1],parity_bit}: {psw_in[2],psw_in[1],now_mem_208[5:3],psw_in[0],now_mem_208[1],parity_bit};
    assign next_mem_209 = ((w_addr == 8'd209))?   w_data: now_mem_209;
    assign next_mem_210 = ((w_addr == 8'd210))?   w_data: now_mem_210;
    assign next_mem_211 = ((w_addr == 8'd211))?   w_data: now_mem_211;
    assign next_mem_212 = ((w_addr == 8'd212))?   w_data: now_mem_212;
    assign next_mem_213 = ((w_addr == 8'd213))?   w_data: now_mem_213;
    assign next_mem_214 = ((w_addr == 8'd214))?   w_data: now_mem_214;
    assign next_mem_215 = ((w_addr == 8'd215))?   w_data: now_mem_215;
    assign next_mem_216 = ((w_addr == 8'd216))?   w_data: now_mem_216;
    assign next_mem_217 = ((w_addr == 8'd217))?   w_data: now_mem_217;
    assign next_mem_218 = ((w_addr == 8'd218))?   w_data: now_mem_218;
    assign next_mem_219 = ((w_addr == 8'd219))?   w_data: now_mem_219;
    assign next_mem_220 = ((w_addr == 8'd220))?   w_data: now_mem_220;
    assign next_mem_221 = ((w_addr == 8'd221))?   w_data: now_mem_221;
    assign next_mem_222 = ((w_addr == 8'd222))?   w_data: now_mem_222;
    assign next_mem_223 = ((w_addr == 8'd223))?   w_data: now_mem_223;
    assign next_mem_224 = ((w_addr == 8'd224))?   w_data: now_mem_224;
    assign next_mem_225 = ((w_addr == 8'd225))?   w_data: now_mem_225;
    assign next_mem_226 = ((w_addr == 8'd226))?   w_data: now_mem_226;
    assign next_mem_227 = ((w_addr == 8'd227))?   w_data: now_mem_227;
    assign next_mem_228 = ((w_addr == 8'd228))?   w_data: now_mem_228;
    assign next_mem_229 = ((w_addr == 8'd229))?   w_data: now_mem_229;
    assign next_mem_230 = ((w_addr == 8'd230))?   w_data: now_mem_230;
    assign next_mem_231 = ((w_addr == 8'd231))?   w_data: now_mem_231;
    assign next_mem_232 = ((w_addr == 8'd232))?   w_data: now_mem_232;
    assign next_mem_233 = ((w_addr == 8'd233))?   w_data: now_mem_233;
    assign next_mem_234 = ((w_addr == 8'd234))?   w_data: now_mem_234;
    assign next_mem_235 = ((w_addr == 8'd235))?   w_data: now_mem_235;
    assign next_mem_236 = ((w_addr == 8'd236))?   w_data: now_mem_236;
    assign next_mem_237 = ((w_addr == 8'd237))?   w_data: now_mem_237;
    assign next_mem_238 = ((w_addr == 8'd238))?   w_data: now_mem_238;
    assign next_mem_239 = ((w_addr == 8'd239))?   w_data: now_mem_239;
    assign next_mem_240 = ((w_addr == 8'd240))?   w_data: now_mem_240;
    assign next_mem_241 = ((w_addr == 8'd241))?   w_data: now_mem_241;
    assign next_mem_242 = ((w_addr == 8'd242))?   w_data: now_mem_242;
    assign next_mem_243 = ((w_addr == 8'd243))?   w_data: now_mem_243;
    assign next_mem_244 = ((w_addr == 8'd244))?   w_data: now_mem_244;
    assign next_mem_245 = ((w_addr == 8'd245))?   w_data: now_mem_245;
    assign next_mem_246 = ((w_addr == 8'd246))?   w_data: now_mem_246;
    assign next_mem_247 = ((w_addr == 8'd247))?   w_data: now_mem_247;
    assign next_mem_248 = ((w_addr == 8'd248))?   w_data: now_mem_248;
    assign next_mem_249 = ((w_addr == 8'd249))?   w_data: now_mem_249;
    assign next_mem_250 = ((w_addr == 8'd250))?   w_data: now_mem_250;
    assign next_mem_251 = ((w_addr == 8'd251))?   w_data: now_mem_251;
    assign next_mem_252 = ((w_addr == 8'd252))?   w_data: now_mem_252;
    assign next_mem_253 = ((w_addr == 8'd253))?   w_data: now_mem_253;
    assign next_mem_254 = ((w_addr == 8'd254))?   w_data: now_mem_254;
    assign next_mem_255 = ((w_addr == 8'd255))?   w_data: now_mem_255;
       
    
    
    always@(posedge clk) begin
        if(reset) begin
            // `SFR_PSW     <=   8'h00;
            `SFR_SP      <=   8'h07;
            `SFR_P0      <=   8'hFF;
            `SFR_P1      <=   8'hFF;
            `SFR_P2      <=   8'hFF;
            `SFR_P3      <=   8'hFF;
            // `SFR_ACC     <=   8'h00;
            // `SFR_B       <=   8'h00;
            // `SFR_DPH     <=   8'h00;
            // `SFR_DPL     <=   8'h00;
            // `SFR_IE      <=   8'h00;
            // `SFR_IP      <=   8'h00;
            // `SFR_SCON    <=   8'h00;
            // `SFR_SBUF    <=   8'h00;
            // `SFR_TCON    <=   8'h00;
            // `SFR_TMOD    <=   8'h00;
            // `SFR_TH0     <=   8'h00;
            // `SFR_TH1     <=   8'h00;
            // `SFR_TL0     <=   8'h00;
            // `SFR_TL1     <=   8'h00;
            now_mem_0       <=0;
            now_mem_1       <=0;
            now_mem_2       <=0;
            now_mem_3       <=0;
            now_mem_4       <=0;
            now_mem_5       <=0;
            now_mem_6       <=0;
            now_mem_7       <=0;
            now_mem_8       <=0;
            now_mem_9       <=0;
            now_mem_10      <=0;
            now_mem_11      <=0;
            now_mem_12      <=0;
            now_mem_13      <=0;
            now_mem_14      <=0;
            now_mem_15      <=0;
            now_mem_16      <=0;
            now_mem_17      <=0;
            now_mem_18      <=0;
            now_mem_19      <=0;
            now_mem_20      <=0;
            now_mem_21      <=0;
            now_mem_22      <=0;
            now_mem_23      <=0;
            now_mem_24      <=0;
            now_mem_25      <=0;
            now_mem_26      <=0;
            now_mem_27      <=0;
            now_mem_28      <=0;
            now_mem_29      <=0;
            now_mem_30      <=0;
            now_mem_31      <=0;
            now_mem_32      <=0;
            now_mem_33      <=0;
            now_mem_34      <=0;
            now_mem_35      <=0;
            now_mem_36      <=0;
            now_mem_37      <=0;
            now_mem_38      <=0;
            now_mem_39      <=0;
            now_mem_40      <=0;
            now_mem_41      <=0;
            now_mem_42      <=0;
            now_mem_43      <=0;
            now_mem_44      <=0;
            now_mem_45      <=0;
            now_mem_46      <=0;
            now_mem_47      <=0;
            now_mem_48      <=0;
            now_mem_49      <=0;
            now_mem_50      <=0;
            now_mem_51      <=0;
            now_mem_52      <=0;
            now_mem_53      <=0;
            now_mem_54      <=0;
            now_mem_55      <=0;
            now_mem_56      <=0;
            now_mem_57      <=0;
            now_mem_58      <=0;
            now_mem_59      <=0;
            now_mem_60      <=0;
            now_mem_61      <=0;
            now_mem_62      <=0;
            now_mem_63      <=0;
            now_mem_64      <=0;
            now_mem_65      <=0;
            now_mem_66      <=0;
            now_mem_67      <=0;
            now_mem_68      <=0;
            now_mem_69      <=0;
            now_mem_70      <=0;
            now_mem_71      <=0;
            now_mem_72      <=0;
            now_mem_73      <=0;
            now_mem_74      <=0;
            now_mem_75      <=0;
            now_mem_76      <=0;
            now_mem_77      <=0;
            now_mem_78      <=0;
            now_mem_79      <=0;
            now_mem_80      <=0;
            now_mem_81      <=0;
            now_mem_82      <=0;
            now_mem_83      <=0;
            now_mem_84      <=0;
            now_mem_85      <=0;
            now_mem_86      <=0;
            now_mem_87      <=0;
            now_mem_88      <=0;
            now_mem_89      <=0;
            now_mem_90      <=0;
            now_mem_91      <=0;
            now_mem_92      <=0;
            now_mem_93      <=0;
            now_mem_94      <=0;
            now_mem_95      <=0;
            now_mem_96      <=0;
            now_mem_97      <=0;
            now_mem_98      <=0;
            now_mem_99      <=0;
            now_mem_100     <=0;
            now_mem_101     <=0;
            now_mem_102     <=0;
            now_mem_103     <=0;
            now_mem_104     <=0;
            now_mem_105     <=0;
            now_mem_106     <=0;
            now_mem_107     <=0;
            now_mem_108     <=0;
            now_mem_109     <=0;
            now_mem_110     <=0;
            now_mem_111     <=0;
            now_mem_112     <=0;
            now_mem_113     <=0;
            now_mem_114     <=0;
            now_mem_115     <=0;
            now_mem_116     <=0;
            now_mem_117     <=0;
            now_mem_118     <=0;
            now_mem_119     <=0;
            now_mem_120     <=0;
            now_mem_121     <=0;
            now_mem_122     <=0;
            now_mem_123     <=0;
            now_mem_124     <=0;
            now_mem_125     <=0;
            now_mem_126     <=0;
            now_mem_127     <=0;
            now_mem_130     <=0;
            now_mem_131     <=0;
            now_mem_132     <=0;
            now_mem_133     <=0;
            now_mem_134     <=0;
            now_mem_135     <=0;
            now_mem_136     <=0;
            now_mem_137     <=0;
            // now_mem_138     <=0;
            // now_mem_139     <=0;
            // now_mem_140     <=0;
            // now_mem_141     <=0;
            now_mem_142     <=0;
            now_mem_143     <=0;
            now_mem_145     <=0;
            now_mem_146     <=0;
            now_mem_147     <=0;
            now_mem_148     <=0;
            now_mem_149     <=0;
            now_mem_150     <=0;
            now_mem_151     <=0;
            now_mem_152     <=0;
            now_mem_153     <=0;
            now_mem_154     <=0;
            now_mem_155     <=0;
            now_mem_156     <=0;
            now_mem_157     <=0;
            now_mem_158     <=0;
            now_mem_159     <=0;
            now_mem_161     <=0;
            now_mem_162     <=0;
            now_mem_163     <=0;
            now_mem_164     <=0;
            now_mem_165     <=0;
            now_mem_166     <=0;
            now_mem_167     <=0;
            now_mem_168     <=0;
            now_mem_169     <=0;
            now_mem_170     <=0;
            now_mem_171     <=0;
            now_mem_172     <=0;
            now_mem_173     <=0;
            now_mem_174     <=0;
            now_mem_175     <=0;
            now_mem_177     <=0;
            now_mem_178     <=0;
            now_mem_179     <=0;
            now_mem_180     <=0;
            now_mem_181     <=0;
            now_mem_182     <=0;
            now_mem_183     <=0;
            now_mem_184     <=0;
            now_mem_185     <=0;
            now_mem_186     <=0;
            now_mem_187     <=0;
            now_mem_188     <=0;
            now_mem_189     <=0;
            now_mem_190     <=0;
            now_mem_191     <=0;
            now_mem_192     <=0;
            now_mem_193     <=0;
            now_mem_194     <=0;
            now_mem_195     <=0;
            now_mem_196     <=0;
            now_mem_197     <=0;
            now_mem_198     <=0;
            now_mem_199     <=0;
            now_mem_200     <=0;
            now_mem_201     <=0;
            now_mem_202     <=0;
            now_mem_203     <=0;
            now_mem_204     <=0;
            now_mem_205     <=0;
            now_mem_206     <=0;
            now_mem_207     <=0;
            now_mem_208     <=0;
            now_mem_209     <=0;
            now_mem_210     <=0;
            now_mem_211     <=0;
            now_mem_212     <=0;
            now_mem_213     <=0;
            now_mem_214     <=0;
            now_mem_215     <=0;
            now_mem_216     <=0;
            now_mem_217     <=0;
            now_mem_218     <=0;
            now_mem_219     <=0;
            now_mem_220     <=0;
            now_mem_221     <=0;
            now_mem_222     <=0;
            now_mem_223     <=0;
            now_mem_224     <=0;
            now_mem_225     <=0;
            now_mem_226     <=0;
            now_mem_227     <=0;
            now_mem_228     <=0;
            now_mem_229     <=0;
            now_mem_230     <=0;
            now_mem_231     <=0;
            now_mem_232     <=0;
            now_mem_233     <=0;
            now_mem_234     <=0;
            now_mem_235     <=0;
            now_mem_236     <=0;
            now_mem_237     <=0;
            now_mem_238     <=0;
            now_mem_239     <=0;
            now_mem_240     <=0;
            now_mem_241     <=0;
            now_mem_242     <=0;
            now_mem_243     <=0;
            now_mem_244     <=0;
            now_mem_245     <=0;
            now_mem_246     <=0;
            now_mem_247     <=0;
            now_mem_248     <=0;
            now_mem_249     <=0;
            now_mem_250     <=0;
            now_mem_251     <=0;
            now_mem_252     <=0;
            now_mem_253     <=0;
            now_mem_254     <=0;
            now_mem_255     <=0;
            
        
        end
        else begin
            if(mem_write_en_in) begin
                now_mem_0   <=  next_mem_0      ;
                now_mem_1   <=  next_mem_1      ;
                now_mem_2   <=  next_mem_2      ;
                now_mem_3   <=  next_mem_3      ;
                now_mem_4   <=  next_mem_4      ;
                now_mem_5   <=  next_mem_5      ;
                now_mem_6   <=  next_mem_6      ;
                now_mem_7   <=  next_mem_7      ;
                now_mem_8   <=  next_mem_8      ;
                now_mem_9   <=  next_mem_9      ;
                now_mem_10  <=  next_mem_10     ;
                now_mem_11  <=  next_mem_11     ;
                now_mem_12  <=  next_mem_12     ;
                now_mem_13  <=  next_mem_13     ;
                now_mem_14  <=  next_mem_14     ;
                now_mem_15  <=  next_mem_15     ;
                now_mem_16  <=  next_mem_16     ;
                now_mem_17  <=  next_mem_17     ;
                now_mem_18  <=  next_mem_18     ;
                now_mem_19  <=  next_mem_19     ;
                now_mem_20  <=  next_mem_20     ;
                now_mem_21  <=  next_mem_21     ;
                now_mem_22  <=  next_mem_22     ;
                now_mem_23  <=  next_mem_23     ;
                now_mem_24  <=  next_mem_24     ;
                now_mem_25  <=  next_mem_25     ;
                now_mem_26  <=  next_mem_26     ;
                now_mem_27  <=  next_mem_27     ;
                now_mem_28  <=  next_mem_28     ;
                now_mem_29  <=  next_mem_29     ;
                now_mem_30  <=  next_mem_30     ;
                now_mem_31  <=  next_mem_31     ;
                now_mem_32  <=  next_mem_32     ;
                now_mem_33  <=  next_mem_33     ;
                now_mem_34  <=  next_mem_34     ;
                now_mem_35  <=  next_mem_35     ;
                now_mem_36  <=  next_mem_36     ;
                now_mem_37  <=  next_mem_37     ;
                now_mem_38  <=  next_mem_38     ;
                now_mem_39  <=  next_mem_39     ;
                now_mem_40  <=  next_mem_40     ;
                now_mem_41  <=  next_mem_41     ;
                now_mem_42  <=  next_mem_42     ;
                now_mem_43  <=  next_mem_43     ;
                now_mem_44  <=  next_mem_44     ;
                now_mem_45  <=  next_mem_45     ;
                now_mem_46  <=  next_mem_46     ;
                now_mem_47  <=  next_mem_47     ;
                now_mem_48  <=  next_mem_48     ;
                now_mem_49  <=  next_mem_49     ;
                now_mem_50  <=  next_mem_50     ;
                now_mem_51  <=  next_mem_51     ;
                now_mem_52  <=  next_mem_52     ;
                now_mem_53  <=  next_mem_53     ;
                now_mem_54  <=  next_mem_54     ;
                now_mem_55  <=  next_mem_55     ;
                now_mem_56  <=  next_mem_56     ;
                now_mem_57  <=  next_mem_57     ;
                now_mem_58  <=  next_mem_58     ;
                now_mem_59  <=  next_mem_59     ;
                now_mem_60  <=  next_mem_60     ;
                now_mem_61  <=  next_mem_61     ;
                now_mem_62  <=  next_mem_62     ;
                now_mem_63  <=  next_mem_63     ;
                now_mem_64  <=  next_mem_64     ;
                now_mem_65  <=  next_mem_65     ;
                now_mem_66  <=  next_mem_66     ;
                now_mem_67  <=  next_mem_67     ;
                now_mem_68  <=  next_mem_68     ;
                now_mem_69  <=  next_mem_69     ;
                now_mem_70  <=  next_mem_70     ;
                now_mem_71  <=  next_mem_71     ;
                now_mem_72  <=  next_mem_72     ;
                now_mem_73  <=  next_mem_73     ;
                now_mem_74  <=  next_mem_74     ;
                now_mem_75  <=  next_mem_75     ;
                now_mem_76  <=  next_mem_76     ;
                now_mem_77  <=  next_mem_77     ;
                now_mem_78  <=  next_mem_78     ;
                now_mem_79  <=  next_mem_79     ;
                now_mem_80  <=  next_mem_80     ;
                now_mem_81  <=  next_mem_81     ;
                now_mem_82  <=  next_mem_82     ;
                now_mem_83  <=  next_mem_83     ;
                now_mem_84  <=  next_mem_84     ;
                now_mem_85  <=  next_mem_85     ;
                now_mem_86  <=  next_mem_86     ;
                now_mem_87  <=  next_mem_87     ;
                now_mem_88  <=  next_mem_88     ;
                now_mem_89  <=  next_mem_89     ;
                now_mem_90  <=  next_mem_90     ;
                now_mem_91  <=  next_mem_91     ;
                now_mem_92  <=  next_mem_92     ;
                now_mem_93  <=  next_mem_93     ;
                now_mem_94  <=  next_mem_94     ;
                now_mem_95  <=  next_mem_95     ;
                now_mem_96  <=  next_mem_96     ;
                now_mem_97  <=  next_mem_97     ;
                now_mem_98  <=  next_mem_98     ;
                now_mem_99  <=  next_mem_99     ;
                now_mem_100 <=  next_mem_100    ;
                now_mem_101 <=  next_mem_101    ;
                now_mem_102 <=  next_mem_102    ;
                now_mem_103 <=  next_mem_103    ;
                now_mem_104 <=  next_mem_104    ;
                now_mem_105 <=  next_mem_105    ;
                now_mem_106 <=  next_mem_106    ;
                now_mem_107 <=  next_mem_107    ;
                now_mem_108 <=  next_mem_108    ;
                now_mem_109 <=  next_mem_109    ;
                now_mem_110 <=  next_mem_110    ;
                now_mem_111 <=  next_mem_111    ;
                now_mem_112 <=  next_mem_112    ;
                now_mem_113 <=  next_mem_113    ;
                now_mem_114 <=  next_mem_114    ;
                now_mem_115 <=  next_mem_115    ;
                now_mem_116 <=  next_mem_116    ;
                now_mem_117 <=  next_mem_117    ;
                now_mem_118 <=  next_mem_118    ;
                now_mem_119 <=  next_mem_119    ;
                now_mem_120 <=  next_mem_120    ;
                now_mem_121 <=  next_mem_121    ;
                now_mem_122 <=  next_mem_122    ;
                now_mem_123 <=  next_mem_123    ;
                now_mem_124 <=  next_mem_124    ;
                now_mem_125 <=  next_mem_125    ;
                now_mem_126 <=  next_mem_126    ;
                now_mem_127 <=  next_mem_127    ;
                now_mem_128 <=  next_mem_128    ;
                //now_mem_129 <=  next_mem_129    ;     need to write to SP when mem_write_en is low, (sp_dec)
                now_mem_130 <=  next_mem_130    ;
                now_mem_131 <=  next_mem_131    ;
                now_mem_132 <=  next_mem_132    ;
                now_mem_133 <=  next_mem_133    ;
                now_mem_134 <=  next_mem_134    ;
                now_mem_135 <=  next_mem_135    ;
                now_mem_136 <=  next_mem_136    ;
                now_mem_137 <=  next_mem_137    ;
                // now_mem_138 <=  next_mem_138    ;
                // now_mem_139 <=  next_mem_139    ;
                // now_mem_140 <=  next_mem_140    ;
                // now_mem_141 <=  next_mem_141    ;
                now_mem_142 <=  next_mem_142    ;
                now_mem_143 <=  next_mem_143    ;
                now_mem_144 <=  next_mem_144    ;
                now_mem_145 <=  next_mem_145    ;
                now_mem_146 <=  next_mem_146    ;
                now_mem_147 <=  next_mem_147    ;
                now_mem_148 <=  next_mem_148    ;
                now_mem_149 <=  next_mem_149    ;
                now_mem_150 <=  next_mem_150    ;
                now_mem_151 <=  next_mem_151    ;
                //now_mem_152 <=  next_mem_152    ;
                //now_mem_153 <=  next_mem_153    ;
                now_mem_154 <=  next_mem_154    ;
                now_mem_155 <=  next_mem_155    ;
                now_mem_156 <=  next_mem_156    ;
                now_mem_157 <=  next_mem_157    ;
                now_mem_158 <=  next_mem_158    ;
                now_mem_159 <=  next_mem_159    ;
                now_mem_160 <=  next_mem_160    ;
                now_mem_161 <=  next_mem_161    ;
                now_mem_162 <=  next_mem_162    ;
                now_mem_163 <=  next_mem_163    ;
                now_mem_164 <=  next_mem_164    ;
                now_mem_165 <=  next_mem_165    ;
                now_mem_166 <=  next_mem_166    ;
                now_mem_167 <=  next_mem_167    ;
                now_mem_168 <=  next_mem_168    ;
                now_mem_169 <=  next_mem_169    ;
                now_mem_170 <=  next_mem_170    ;
                now_mem_171 <=  next_mem_171    ;
                now_mem_172 <=  next_mem_172    ;
                now_mem_173 <=  next_mem_173    ;
                now_mem_174 <=  next_mem_174    ;
                now_mem_175 <=  next_mem_175    ;
                now_mem_176 <=  next_mem_176    ;
                now_mem_177 <=  next_mem_177    ;
                now_mem_178 <=  next_mem_178    ;
                now_mem_179 <=  next_mem_179    ;
                now_mem_180 <=  next_mem_180    ;
                now_mem_181 <=  next_mem_181    ;
                now_mem_182 <=  next_mem_182    ;
                now_mem_183 <=  next_mem_183    ;
                now_mem_184 <=  next_mem_184    ;
                now_mem_185 <=  next_mem_185    ;
                now_mem_186 <=  next_mem_186    ;
                now_mem_187 <=  next_mem_187    ;
                now_mem_188 <=  next_mem_188    ;
                now_mem_189 <=  next_mem_189    ;
                now_mem_190 <=  next_mem_190    ;
                now_mem_191 <=  next_mem_191    ;
                now_mem_192 <=  next_mem_192    ;
                now_mem_193 <=  next_mem_193    ;
                now_mem_194 <=  next_mem_194    ;
                now_mem_195 <=  next_mem_195    ;
                now_mem_196 <=  next_mem_196    ;
                now_mem_197 <=  next_mem_197    ;
                now_mem_198 <=  next_mem_198    ;
                now_mem_199 <=  next_mem_199    ;
                now_mem_200 <=  next_mem_200    ;
                now_mem_201 <=  next_mem_201    ;
                now_mem_202 <=  next_mem_202    ;
                now_mem_203 <=  next_mem_203    ;
                now_mem_204 <=  next_mem_204    ;
                now_mem_205 <=  next_mem_205    ;
                now_mem_206 <=  next_mem_206    ;
                now_mem_207 <=  next_mem_207    ;
                // psw unconditionally registered
                now_mem_209 <=  next_mem_209    ;
                now_mem_210 <=  next_mem_210    ;
                now_mem_211 <=  next_mem_211    ;
                now_mem_212 <=  next_mem_212    ;
                now_mem_213 <=  next_mem_213    ;
                now_mem_214 <=  next_mem_214    ;
                now_mem_215 <=  next_mem_215    ;
                now_mem_216 <=  next_mem_216    ;
                now_mem_217 <=  next_mem_217    ;
                now_mem_218 <=  next_mem_218    ;
                now_mem_219 <=  next_mem_219    ;
                now_mem_220 <=  next_mem_220    ;
                now_mem_221 <=  next_mem_221    ;
                now_mem_222 <=  next_mem_222    ;
                now_mem_223 <=  next_mem_223    ;
                now_mem_224 <=  next_mem_224    ;
                now_mem_225 <=  next_mem_225    ;
                now_mem_226 <=  next_mem_226    ;
                now_mem_227 <=  next_mem_227    ;
                now_mem_228 <=  next_mem_228    ;
                now_mem_229 <=  next_mem_229    ;
                now_mem_230 <=  next_mem_230    ;
                now_mem_231 <=  next_mem_231    ;
                now_mem_232 <=  next_mem_232    ;
                now_mem_233 <=  next_mem_233    ;
                now_mem_234 <=  next_mem_234    ;
                now_mem_235 <=  next_mem_235    ;
                now_mem_236 <=  next_mem_236    ;
                now_mem_237 <=  next_mem_237    ;
                now_mem_238 <=  next_mem_238    ;
                now_mem_239 <=  next_mem_239    ;
                now_mem_240 <=  next_mem_240    ;
                now_mem_241 <=  next_mem_241    ;
                now_mem_242 <=  next_mem_242    ;
                now_mem_243 <=  next_mem_243    ;
                now_mem_244 <=  next_mem_244    ;
                now_mem_245 <=  next_mem_245    ;
                now_mem_246 <=  next_mem_246    ;
                now_mem_247 <=  next_mem_247    ;
                now_mem_248 <=  next_mem_248    ;
                now_mem_249 <=  next_mem_249    ;
                now_mem_250 <=  next_mem_250    ;
                now_mem_251 <=  next_mem_251    ;
                now_mem_252 <=  next_mem_252    ;
                now_mem_253 <=  next_mem_253    ;
                now_mem_254 <=  next_mem_254    ;
                now_mem_255 <=  next_mem_255    ;
            end
            if(mem_write_en_in | sp_dec_in) begin
                now_mem_129 <= next_mem_129;
            end
            
            if(mem_write_en_in | serial_rx_done_in | serial_tx_done_in ) begin
                now_mem_152 <=  next_mem_152;
            end
            
            if( mem_write_en_in | next_mem_152[0] ) begin
                now_mem_153 <=  next_mem_153    ;
            end
            
            now_mem_208 <=  next_mem_208    ;       // psw
         end
    end
    
    assign r_data0_out = now_mem_224;


    
    
    always@* begin
        case(r_addr1_d) 
              0: begin r_data1_out = now_mem_0      ; end
              1: begin r_data1_out = now_mem_1      ; end
              2: begin r_data1_out = now_mem_2      ; end
              3: begin r_data1_out = now_mem_3      ; end
              4: begin r_data1_out = now_mem_4      ; end
              5: begin r_data1_out = now_mem_5      ; end
              6: begin r_data1_out = now_mem_6      ; end
              7: begin r_data1_out = now_mem_7      ; end
              8: begin r_data1_out = now_mem_8      ; end
              9: begin r_data1_out = now_mem_9      ; end
             10: begin r_data1_out = now_mem_10     ; end
             11: begin r_data1_out = now_mem_11     ; end
             12: begin r_data1_out = now_mem_12     ; end
             13: begin r_data1_out = now_mem_13     ; end
             14: begin r_data1_out = now_mem_14     ; end
             15: begin r_data1_out = now_mem_15     ; end
             16: begin r_data1_out = now_mem_16     ; end
             17: begin r_data1_out = now_mem_17     ; end
             18: begin r_data1_out = now_mem_18     ; end
             19: begin r_data1_out = now_mem_19     ; end
             20: begin r_data1_out = now_mem_20     ; end
             21: begin r_data1_out = now_mem_21     ; end
             22: begin r_data1_out = now_mem_22     ; end
             23: begin r_data1_out = now_mem_23     ; end
             24: begin r_data1_out = now_mem_24     ; end
             25: begin r_data1_out = now_mem_25     ; end
             26: begin r_data1_out = now_mem_26     ; end
             27: begin r_data1_out = now_mem_27     ; end
             28: begin r_data1_out = now_mem_28     ; end
             29: begin r_data1_out = now_mem_29     ; end
             30: begin r_data1_out = now_mem_30     ; end
             31: begin r_data1_out = now_mem_31     ; end
             32: begin r_data1_out = now_mem_32     ; end
             33: begin r_data1_out = now_mem_33     ; end
             34: begin r_data1_out = now_mem_34     ; end
             35: begin r_data1_out = now_mem_35     ; end
             36: begin r_data1_out = now_mem_36     ; end
             37: begin r_data1_out = now_mem_37     ; end
             38: begin r_data1_out = now_mem_38     ; end
             39: begin r_data1_out = now_mem_39     ; end
             40: begin r_data1_out = now_mem_40     ; end
             41: begin r_data1_out = now_mem_41     ; end
             42: begin r_data1_out = now_mem_42     ; end
             43: begin r_data1_out = now_mem_43     ; end
             44: begin r_data1_out = now_mem_44     ; end
             45: begin r_data1_out = now_mem_45     ; end
             46: begin r_data1_out = now_mem_46     ; end
             47: begin r_data1_out = now_mem_47     ; end
             48: begin r_data1_out = now_mem_48     ; end
             49: begin r_data1_out = now_mem_49     ; end
             50: begin r_data1_out = now_mem_50     ; end
             51: begin r_data1_out = now_mem_51     ; end
             52: begin r_data1_out = now_mem_52     ; end
             53: begin r_data1_out = now_mem_53     ; end
             54: begin r_data1_out = now_mem_54     ; end
             55: begin r_data1_out = now_mem_55     ; end
             56: begin r_data1_out = now_mem_56     ; end
             57: begin r_data1_out = now_mem_57     ; end
             58: begin r_data1_out = now_mem_58     ; end
             59: begin r_data1_out = now_mem_59     ; end
             60: begin r_data1_out = now_mem_60     ; end
             61: begin r_data1_out = now_mem_61     ; end
             62: begin r_data1_out = now_mem_62     ; end
             63: begin r_data1_out = now_mem_63     ; end
             64: begin r_data1_out = now_mem_64     ; end
             65: begin r_data1_out = now_mem_65     ; end
             66: begin r_data1_out = now_mem_66     ; end
             67: begin r_data1_out = now_mem_67     ; end
             68: begin r_data1_out = now_mem_68     ; end
             69: begin r_data1_out = now_mem_69     ; end
             70: begin r_data1_out = now_mem_70     ; end
             71: begin r_data1_out = now_mem_71     ; end
             72: begin r_data1_out = now_mem_72     ; end
             73: begin r_data1_out = now_mem_73     ; end
             74: begin r_data1_out = now_mem_74     ; end
             75: begin r_data1_out = now_mem_75     ; end
             76: begin r_data1_out = now_mem_76     ; end
             77: begin r_data1_out = now_mem_77     ; end
             78: begin r_data1_out = now_mem_78     ; end
             79: begin r_data1_out = now_mem_79     ; end
             80: begin r_data1_out = now_mem_80     ; end
             81: begin r_data1_out = now_mem_81     ; end
             82: begin r_data1_out = now_mem_82     ; end
             83: begin r_data1_out = now_mem_83     ; end
             84: begin r_data1_out = now_mem_84     ; end
             85: begin r_data1_out = now_mem_85     ; end
             86: begin r_data1_out = now_mem_86     ; end
             87: begin r_data1_out = now_mem_87     ; end
             88: begin r_data1_out = now_mem_88     ; end
             89: begin r_data1_out = now_mem_89     ; end
             90: begin r_data1_out = now_mem_90     ; end
             91: begin r_data1_out = now_mem_91     ; end
             92: begin r_data1_out = now_mem_92     ; end
             93: begin r_data1_out = now_mem_93     ; end
             94: begin r_data1_out = now_mem_94     ; end
             95: begin r_data1_out = now_mem_95     ; end
             96: begin r_data1_out = now_mem_96     ; end
             97: begin r_data1_out = now_mem_97     ; end
             98: begin r_data1_out = now_mem_98     ; end
             99: begin r_data1_out = now_mem_99     ; end
            100: begin r_data1_out = now_mem_100    ; end
            101: begin r_data1_out = now_mem_101    ; end
            102: begin r_data1_out = now_mem_102    ; end
            103: begin r_data1_out = now_mem_103    ; end
            104: begin r_data1_out = now_mem_104    ; end
            105: begin r_data1_out = now_mem_105    ; end
            106: begin r_data1_out = now_mem_106    ; end
            107: begin r_data1_out = now_mem_107    ; end
            108: begin r_data1_out = now_mem_108    ; end
            109: begin r_data1_out = now_mem_109    ; end
            110: begin r_data1_out = now_mem_110    ; end
            111: begin r_data1_out = now_mem_111    ; end
            112: begin r_data1_out = now_mem_112    ; end
            113: begin r_data1_out = now_mem_113    ; end
            114: begin r_data1_out = now_mem_114    ; end
            115: begin r_data1_out = now_mem_115    ; end
            116: begin r_data1_out = now_mem_116    ; end
            117: begin r_data1_out = now_mem_117    ; end
            118: begin r_data1_out = now_mem_118    ; end
            119: begin r_data1_out = now_mem_119    ; end
            120: begin r_data1_out = now_mem_120    ; end
            121: begin r_data1_out = now_mem_121    ; end
            122: begin r_data1_out = now_mem_122    ; end
            123: begin r_data1_out = now_mem_123    ; end
            124: begin r_data1_out = now_mem_124    ; end
            125: begin r_data1_out = now_mem_125    ; end
            126: begin r_data1_out = now_mem_126    ; end
            127: begin r_data1_out = now_mem_127    ; end
            128: begin r_data1_out = (io_port_en_in) ? port0_in: now_mem_128    ; end
            129: begin r_data1_out = now_mem_129    ; end
            130: begin r_data1_out = now_mem_130    ; end
            131: begin r_data1_out = now_mem_131    ; end
            132: begin r_data1_out = now_mem_132    ; end
            133: begin r_data1_out = now_mem_133    ; end
            134: begin r_data1_out = now_mem_134    ; end
            135: begin r_data1_out = now_mem_135    ; end
            136: begin r_data1_out = now_mem_136    ; end
            137: begin r_data1_out = now_mem_137    ; end
            138: begin r_data1_out = now_mem_138    ; end
            139: begin r_data1_out = now_mem_139    ; end
            140: begin r_data1_out = now_mem_140    ; end
            141: begin r_data1_out = now_mem_141    ; end
            142: begin r_data1_out = now_mem_142    ; end
            143: begin r_data1_out = now_mem_143    ; end
            144: begin r_data1_out = (io_port_en_in) ? port1_in: now_mem_144    ; end
            145: begin r_data1_out = now_mem_145    ; end
            146: begin r_data1_out = now_mem_146    ; end
            147: begin r_data1_out = now_mem_147    ; end
            148: begin r_data1_out = now_mem_148    ; end
            149: begin r_data1_out = now_mem_149    ; end
            150: begin r_data1_out = now_mem_150    ; end
            151: begin r_data1_out = now_mem_151    ; end
            152: begin r_data1_out = now_mem_152    ; end
            153: begin r_data1_out = now_mem_153    ; end
            154: begin r_data1_out = now_mem_154    ; end
            155: begin r_data1_out = now_mem_155    ; end
            156: begin r_data1_out = now_mem_156    ; end
            157: begin r_data1_out = now_mem_157    ; end
            158: begin r_data1_out = now_mem_158    ; end
            159: begin r_data1_out = now_mem_159    ; end
            160: begin r_data1_out = (io_port_en_in) ? port2_in: now_mem_160    ; end
            161: begin r_data1_out = now_mem_161    ; end
            162: begin r_data1_out = now_mem_162    ; end
            163: begin r_data1_out = now_mem_163    ; end
            164: begin r_data1_out = now_mem_164    ; end
            165: begin r_data1_out = now_mem_165    ; end
            166: begin r_data1_out = now_mem_166    ; end
            167: begin r_data1_out = now_mem_167    ; end
            168: begin r_data1_out = now_mem_168    ; end
            169: begin r_data1_out = now_mem_169    ; end
            170: begin r_data1_out = now_mem_170    ; end
            171: begin r_data1_out = now_mem_171    ; end
            172: begin r_data1_out = now_mem_172    ; end
            173: begin r_data1_out = now_mem_173    ; end
            174: begin r_data1_out = now_mem_174    ; end
            175: begin r_data1_out = now_mem_175    ; end
            176: begin r_data1_out = (io_port_en_in) ? port3_in: now_mem_176    ; end
            177: begin r_data1_out = now_mem_177    ; end
            178: begin r_data1_out = now_mem_178    ; end
            179: begin r_data1_out = now_mem_179    ; end
            180: begin r_data1_out = now_mem_180    ; end
            181: begin r_data1_out = now_mem_181    ; end
            182: begin r_data1_out = now_mem_182    ; end
            183: begin r_data1_out = now_mem_183    ; end
            184: begin r_data1_out = now_mem_184    ; end
            185: begin r_data1_out = now_mem_185    ; end
            186: begin r_data1_out = now_mem_186    ; end
            187: begin r_data1_out = now_mem_187    ; end
            188: begin r_data1_out = now_mem_188    ; end
            189: begin r_data1_out = now_mem_189    ; end
            190: begin r_data1_out = now_mem_190    ; end
            191: begin r_data1_out = now_mem_191    ; end
            192: begin r_data1_out = now_mem_192    ; end
            193: begin r_data1_out = now_mem_193    ; end
            194: begin r_data1_out = now_mem_194    ; end
            195: begin r_data1_out = now_mem_195    ; end
            196: begin r_data1_out = now_mem_196    ; end
            197: begin r_data1_out = now_mem_197    ; end
            198: begin r_data1_out = now_mem_198    ; end
            199: begin r_data1_out = now_mem_199    ; end
            200: begin r_data1_out = now_mem_200    ; end
            201: begin r_data1_out = now_mem_201    ; end
            202: begin r_data1_out = now_mem_202    ; end
            203: begin r_data1_out = now_mem_203    ; end
            204: begin r_data1_out = now_mem_204    ; end
            205: begin r_data1_out = now_mem_205    ; end
            206: begin r_data1_out = now_mem_206    ; end
            207: begin r_data1_out = now_mem_207    ; end
            208: begin r_data1_out = now_mem_208    ; end
            209: begin r_data1_out = now_mem_209    ; end
            210: begin r_data1_out = now_mem_210    ; end
            211: begin r_data1_out = now_mem_211    ; end
            212: begin r_data1_out = now_mem_212    ; end
            213: begin r_data1_out = now_mem_213    ; end
            214: begin r_data1_out = now_mem_214    ; end
            215: begin r_data1_out = now_mem_215    ; end
            216: begin r_data1_out = now_mem_216    ; end
            217: begin r_data1_out = now_mem_217    ; end
            218: begin r_data1_out = now_mem_218    ; end
            219: begin r_data1_out = now_mem_219    ; end
            220: begin r_data1_out = now_mem_220    ; end
            221: begin r_data1_out = now_mem_221    ; end
            222: begin r_data1_out = now_mem_222    ; end
            223: begin r_data1_out = now_mem_223    ; end
            224: begin r_data1_out = now_mem_224    ; end
            225: begin r_data1_out = now_mem_225    ; end
            226: begin r_data1_out = now_mem_226    ; end
            227: begin r_data1_out = now_mem_227    ; end
            228: begin r_data1_out = now_mem_228    ; end
            229: begin r_data1_out = now_mem_229    ; end
            230: begin r_data1_out = now_mem_230    ; end
            231: begin r_data1_out = now_mem_231    ; end
            232: begin r_data1_out = now_mem_232    ; end
            233: begin r_data1_out = now_mem_233    ; end
            234: begin r_data1_out = now_mem_234    ; end
            235: begin r_data1_out = now_mem_235    ; end
            236: begin r_data1_out = now_mem_236    ; end
            237: begin r_data1_out = now_mem_237    ; end
            238: begin r_data1_out = now_mem_238    ; end
            239: begin r_data1_out = now_mem_239    ; end
            240: begin r_data1_out = now_mem_240    ; end
            241: begin r_data1_out = now_mem_241    ; end
            242: begin r_data1_out = now_mem_242    ; end
            243: begin r_data1_out = now_mem_243    ; end
            244: begin r_data1_out = now_mem_244    ; end
            245: begin r_data1_out = now_mem_245    ; end
            246: begin r_data1_out = now_mem_246    ; end
            247: begin r_data1_out = now_mem_247    ; end
            248: begin r_data1_out = now_mem_248    ; end
            249: begin r_data1_out = now_mem_249    ; end
            250: begin r_data1_out = now_mem_250    ; end
            251: begin r_data1_out = now_mem_251    ; end
            252: begin r_data1_out = now_mem_252    ; end
            253: begin r_data1_out = now_mem_253    ; end
            254: begin r_data1_out = now_mem_254    ; end
            255: begin r_data1_out = now_mem_255    ; end
        endcase
    end    
  
  endmodule