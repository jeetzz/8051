`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:30:05 10/22/2013 
// Design Name: 
// Module Name:    eightzerofiveone_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

//`define READMEM

`ifndef READMEM 
module eightzerofiveone_top
    (
        clkin,
        resetn,
        
        p_mem_portw_addr,
        p_mem_portw_data,
        p_mem_portw_en,
        
        //test synthesis
        alu_out,
        
        port0_in,
        port1_in,
        port2_in,
        port3_in,
        port0_out,
        port1_out,
        port2_out,
        port3_out,
        
        serial_rx_in,
        serial_tx_out
    );
//-------------------------------------------------------
// GLOBAL INCLUDES
//-------------------------------------------------------
    
    	`include "def.v"
// initialization----------------------------------------

  
    
//------------------------------------------------------- 
    
//-------------------------------------------------------
// LOCALPARAMS
//-------------------------------------------------------
    
    localparam  LOG2NUM_OF_BANKS        = 2;
    localparam  INSTRUCTION_WIDTH       = 8;
    localparam  P_ADDR_WIDTH            = 16;
    localparam  MEM_SIZE                = 1 << (P_ADDR_WIDTH-LOG2NUM_OF_BANKS);
    
    localparam  PCINC_VAL_WIDTH         = 2; //(options for +0 , +1, +2, +3)
    localparam  control_shifter_WIDTH           = 2; //(options for +0 , +1, +2, +3) 
    localparam  PC_OFFSEL_SEL_WIDTH     = 4; //( 9 options )
    localparam  NX_AD_WIDTH             = 11;
    localparam  CW_WIDTH                = 47;
    
    //Control signals width
    localparam  CONTROL_PC_INCR_WIDTH   = 2;
    localparam  CONTROL_SHIFTER_WIDTH   = 2;
    localparam  CONTROL_PCOFFSET_WIDTH  = 4; //( 9 options )
    localparam  CONTROL_D0D1_LO_WIDTH   = 2;
    localparam  CONTROL_D0D1_L1_WIDTH   = 1;
    localparam  CONTROL_READ_ADDR_WIDTH = 4;
    localparam  CONTROL_READBIT_WIDTH   = 1;
    localparam  CONTROL_IO_WIDTH        = 1;
    localparam  CONTROL_RIN_WIDTH       = 1;
    localparam  CONTROL_ALU_WIDTH       = 6;
    localparam  CONTROL_ALUB_WIDTH      = 3;
    localparam  CONTROL_WSEL_WIDTH      = 1;
    localparam  CONTROL_WADDR_WIDTH     = 4;
    localparam  CONTROL_WBIT_WIDTH      = 1;
    localparam  CONTROL_WEN_WIDTH       = 1;
    localparam  CONTROL_SPDEC_WIDTH     = 1;
    localparam  CONTROL_PCLOWHIGH_WIDTH = 3;
    localparam  CONTROL_POUT_WIDTH      = 1;
    localparam  CONTROL_BRANCHZ_WIDTH   = 1;
    localparam  CONTROL_ADD16_WIDTH     = 1;
    localparam  CONTROL_RETI_WIDTH      = 1;
    localparam  CONTROL_ERROR_WIDTH     = 1;
    localparam  PSW_WIDTH               = 3;
    
 
    
    
    
    
//--------------------------------------------------------
// I/O Signals
//--------------------------------------------------------

    input                               clkin;
    input                               resetn;
    
    input   [P_ADDR_WIDTH - 1:0]        p_mem_portw_addr;
    input   [INSTRUCTION_WIDTH - 1:0]   p_mem_portw_data;
    input                               p_mem_portw_en;
    
    input   [INSTRUCTION_WIDTH - 1:0]   port0_in;
    input   [INSTRUCTION_WIDTH - 1:0]   port1_in;
    input   [INSTRUCTION_WIDTH - 1:0]   port2_in;
    input   [INSTRUCTION_WIDTH - 1:0]   port3_in;
    output  [INSTRUCTION_WIDTH - 1:0]   port0_out;
    output  [INSTRUCTION_WIDTH - 1:0]   port1_out;
    output  [INSTRUCTION_WIDTH - 1:0]   port2_out;
    output  [INSTRUCTION_WIDTH - 1:0]   port3_out;
    
    input                               serial_rx_in;
    output                              serial_tx_out;
    
    //test synthesis
    output  [INSTRUCTION_WIDTH - 1:0]   alu_out;
`else

module eightzerofiveone_top
    (
        clkin,
        resetn,
        
        
        //test synthesis
        alu_out,
        
        port0_in,
        port1_in,
        port2_in,
        port3_in,
        port0_out,
        port1_out,
        port2_out,
        port3_out,
        
        serial_rx_in,
        serial_tx_out
    );
//-------------------------------------------------------
// GLOBAL INCLUDES
//-------------------------------------------------------
    
    	`include "def.v"
// initialization----------------------------------------
    localparam PROGMEM = 344;
    reg [7:0] temp_memory [0:PROGMEM - 1];
    integer i;
    integer idx;
    integer prog_state;
    
    localparam STATE_EXEC = 0;
    localparam STATE_LOAD = 1;
  
    
//------------------------------------------------------- 
    
//-------------------------------------------------------
// LOCALPARAMS
//-------------------------------------------------------
    
    localparam  LOG2NUM_OF_BANKS        = 2;
    localparam  INSTRUCTION_WIDTH       = 8;
    localparam  P_ADDR_WIDTH            = 16;
    localparam  MEM_SIZE                = 1 << (P_ADDR_WIDTH-LOG2NUM_OF_BANKS);
    
    localparam  PCINC_VAL_WIDTH         = 2; //(options for +0 , +1, +2, +3)
    localparam  control_shifter_WIDTH           = 2; //(options for +0 , +1, +2, +3) 
    localparam  PC_OFFSEL_SEL_WIDTH     = 4; //( 9 options )
    localparam  NX_AD_WIDTH             = 11;
    localparam  CW_WIDTH                = 47;
    
    //Control signals width
    localparam  CONTROL_PC_INCR_WIDTH   = 2;
    localparam  CONTROL_SHIFTER_WIDTH   = 2;
    localparam  CONTROL_PCOFFSET_WIDTH  = 4; //( 9 options )
    localparam  CONTROL_D0D1_LO_WIDTH   = 2;
    localparam  CONTROL_D0D1_L1_WIDTH   = 1;
    localparam  CONTROL_READ_ADDR_WIDTH = 4;
    localparam  CONTROL_READBIT_WIDTH   = 1;
    localparam  CONTROL_IO_WIDTH        = 1;
    localparam  CONTROL_RIN_WIDTH       = 1;
    localparam  CONTROL_ALU_WIDTH       = 6;
    localparam  CONTROL_ALUB_WIDTH      = 3;
    localparam  CONTROL_WSEL_WIDTH      = 1;
    localparam  CONTROL_WADDR_WIDTH     = 4;
    localparam  CONTROL_WBIT_WIDTH      = 1;
    localparam  CONTROL_WEN_WIDTH       = 1;
    localparam  CONTROL_SPDEC_WIDTH     = 1;
    localparam  CONTROL_PCLOWHIGH_WIDTH = 3;
    localparam  CONTROL_POUT_WIDTH      = 1;
    localparam  CONTROL_BRANCHZ_WIDTH   = 1;
    localparam  CONTROL_ADD16_WIDTH     = 1;
    localparam  CONTROL_RETI_WIDTH      = 1;
    localparam  CONTROL_ERROR_WIDTH     = 1;
    localparam  PSW_WIDTH               = 3;
    
 
    
    
    
    
//--------------------------------------------------------
// I/O Signals
//--------------------------------------------------------

    input                               clkin;
    input                               resetn;
    
    input   [INSTRUCTION_WIDTH - 1:0]   port0_in;
    input   [INSTRUCTION_WIDTH - 1:0]   port1_in;
    input   [INSTRUCTION_WIDTH - 1:0]   port2_in;
    input   [INSTRUCTION_WIDTH - 1:0]   port3_in;
    output  [INSTRUCTION_WIDTH - 1:0]   port0_out;
    output  [INSTRUCTION_WIDTH - 1:0]   port1_out;
    output  [INSTRUCTION_WIDTH - 1:0]   port2_out;
    output  [INSTRUCTION_WIDTH - 1:0]   port3_out;
    
    input                               serial_rx_in;
    output                              serial_tx_out;
    
    //test synthesis
    output  [INSTRUCTION_WIDTH - 1:0]   alu_out;

        
    reg   [P_ADDR_WIDTH - 1:0]        p_mem_portw_addr;
    reg   [INSTRUCTION_WIDTH - 1:0]   p_mem_portw_data;
    reg                               p_mem_portw_en;
    
`endif    
//--------------------------------------------------------
// Internal Regs and wires
//--------------------------------------------------------
    
    wire                                    clk;
    wire                                    dcm_clk0;
    wire                                    reset;
    
    // Program memory and instruction buffer interface
    wire    [INSTRUCTION_WIDTH - 1:0]       ins_0;
    wire    [INSTRUCTION_WIDTH - 1:0]       ins_1;
    wire    [INSTRUCTION_WIDTH - 1:0]       ins_2;
    wire                                    p_mem_r1_en;
    
    // Data loader and program memory interface
    wire    [P_ADDR_WIDTH - 1:0]            p_addr;
    
    // Aux_adder and program memory interface
    wire    [P_ADDR_WIDTH - 1:0]            p_addr_alt;
    wire    [P_ADDR_WIDTH - 1:0]            p_addr_alt_reg;
    wire    [INSTRUCTION_WIDTH - 1:0]       p_data_alt;
    wire                                    p_mem_r2_en;
    
    // PC and data_loader interface
    wire    [P_ADDR_WIDTH - 1:0]            pc_to_dataloader;
    wire    [P_ADDR_WIDTH - 1:0]            prev_addr_to_pc;
    
    // PC signals
    reg     [P_ADDR_WIDTH - 1:0]            pc_load_data;
    reg     [P_ADDR_WIDTH - 1:0]            pc_inced_data;
    reg     [P_ADDR_WIDTH - 1:0]            pc_inced_data_d;
    
    wire    [P_ADDR_WIDTH/2 - 1:0]          pc_low_data;
    wire    [P_ADDR_WIDTH/2 - 1:0]          pc_high_data;
    
    
    // Signals from instruction buffer
    wire    [INSTRUCTION_WIDTH - 1:0]       ins_buf_to_op;
    wire    [INSTRUCTION_WIDTH - 1:0]       ins_buf_to_op1;
    wire    [INSTRUCTION_WIDTH - 1:0]       ins_buf_to_op2;

    
    // Internal regs in the top module
    reg     [P_ADDR_WIDTH - 1:0]            pc;
    wire    [P_ADDR_WIDTH - 1:0]            pc_jmp;
        
    reg     [INSTRUCTION_WIDTH - 1:0]       op_1d;
    reg     [INSTRUCTION_WIDTH - 1:0]       op_2d;
    
    reg     [INSTRUCTION_WIDTH - 1:0]       op1_1d;
    reg     [INSTRUCTION_WIDTH - 1:0]       op1_2d;
    
    reg     [INSTRUCTION_WIDTH - 1:0]       op2_1d;
    reg     [INSTRUCTION_WIDTH - 1:0]       op2_2d;
    
    reg     [INSTRUCTION_WIDTH - 1:0]       d0;
    reg     [INSTRUCTION_WIDTH - 1:0]       d1;
        
    wire    [CW_WIDTH - 1:0]                cw_in;
    reg     [CW_WIDTH - 1:0]                cw;
    reg     [CW_WIDTH - 1:0]                cw_1d;
    
    // control signals from decoder_logic
    
    wire    [CONTROL_PC_INCR_WIDTH - 1:0]           control_pc_incr;
    wire    [CONTROL_SHIFTER_WIDTH - 1:0]           control_shifter;
    wire    [CONTROL_PCOFFSET_WIDTH - 1:0]          control_pc_offset;
    wire    [CONTROL_D0D1_LO_WIDTH - 1:0]           control_d0_l0;
    wire    [CONTROL_D0D1_L1_WIDTH - 1:0]           control_d0_l1;
    wire    [CONTROL_D0D1_LO_WIDTH - 1:0]           control_d1_l0;
    wire    [CONTROL_D0D1_L1_WIDTH - 1:0]           control_d1_l1;
    wire    [CONTROL_READ_ADDR_WIDTH - 1:0]         control_read_addr;
    wire    [CONTROL_READBIT_WIDTH - 1:0]           control_readbit;
    wire    [CONTROL_RIN_WIDTH - 1:0]               control_rin;
    wire    [CONTROL_ALU_WIDTH - 1:0]               control_alu_op;
    wire    [CONTROL_ALUB_WIDTH - 1:0]              control_alu_b;
    wire    [CONTROL_WSEL_WIDTH - 1:0]              control_write_sel;
    wire    [CONTROL_WADDR_WIDTH - 1:0]             control_waddr;
    wire    [CONTROL_WBIT_WIDTH - 1:0]              control_wbit;
    wire    [CONTROL_WEN_WIDTH - 1:0]               control_wen;
    wire    [CONTROL_SPDEC_WIDTH - 1:0]             control_spdec;
    wire    [CONTROL_PCLOWHIGH_WIDTH - 1:0]         control_pclow;
    wire    [CONTROL_PCLOWHIGH_WIDTH - 1:0]         control_pchigh;
    wire    [CONTROL_POUT_WIDTH - 1:0]              control_pout;
    wire    [CONTROL_BRANCHZ_WIDTH - 1:0]           control_branchz;
    wire    [CONTROL_ADD16_WIDTH - 1:0]             control_add16;
    wire    [CONTROL_RETI_WIDTH - 1:0]              control_reti;
    
    // ALU signals
    wire                                    cz_flag;
    reg     [INSTRUCTION_WIDTH - 1:0]       d0_to_alu;
    reg     [INSTRUCTION_WIDTH - 1:0]       d1_to_alu;
    wire    [INSTRUCTION_WIDTH - 1:0]       data_alu_to_mem_file;
    wire    [PSW_WIDTH - 1:0]               psw_alu_to_mem_file;
    
    // Memory file signals
    wire    [INSTRUCTION_WIDTH - 1:0]       mem_file_to_d0;
    wire    [INSTRUCTION_WIDTH - 1:0]       mem_file_to_d1;
    wire    [P_ADDR_WIDTH - 1:0]            dptr;
    wire    [INSTRUCTION_WIDTH - 1:0]       acc;
    wire    [PSW_WIDTH - 1:0]               psw_mem_file_to_alu;
    
    // wires for serial_rx
    wire                                    serial_enable;
    wire    [INSTRUCTION_WIDTH - 1:0]       rxdata_serial;
    wire                                    serial_rx_done;
    wire                                    serial_ren;
    wire                                    serial_ri_flag;
    
    // wires for serial_tx
    wire    [INSTRUCTION_WIDTH - 1:0]       txdata_serial;
    wire                                    serial_tx_done;
    wire                                    serial_ti_flag;
    wire                                    serial_baud_clk;
    
    
    
    
//--------------------------------------------------------
// Implementation
//--------------------------------------------------------

`ifdef READMEM
    initial begin
        p_mem_portw_en = 1;
        prog_state = STATE_LOAD;
        $readmemh("prog_mem_in.txt",temp_memory);
        idx =0;    
    end 
    
    always @(posedge clk) begin : loading_to_prg_mem
        case(prog_state)
            STATE_LOAD : begin
                if(idx == PROGMEM) begin
                    prog_state <= STATE_EXEC;
                    p_mem_portw_en <= 1'b0;
                end
                else begin
                    p_mem_portw_addr <= idx;
                    p_mem_portw_data <= temp_memory[idx];
                    idx <= idx + 1;
                end
            end
            STATE_EXEC : begin
                
            end
        endcase
    end


`endif


    assign reset = ~resetn;
    assign clk = clkin;
/* 
    DCM_BASE #(
      .CLKDV_DIVIDE(4.0), // Divide by: 1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5
                          //   7.0,7.5,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0 or 16.0
      .CLKFX_DIVIDE(1), // Can be any integer from 1 to 32
      .CLKFX_MULTIPLY(4), // Can be any integer from 2 to 32
      .CLKIN_DIVIDE_BY_2("FALSE"), // TRUE/FALSE to enable CLKIN divide by two feature
      .CLKIN_PERIOD(10.0), // Specify period of input clock in ns from 1.25 to 1000.00
      .CLKOUT_PHASE_SHIFT("NONE"), // Specify phase shift mode of NONE or FIXED
      .CLK_FEEDBACK("1X"), // Specify clock feedback of NONE or 1X
      .DCM_PERFORMANCE_MODE("MAX_SPEED"), // Can be MAX_SPEED or MAX_RANGE
      .DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"), // SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or
                                            //   an integer from 0 to 15
      .DFS_FREQUENCY_MODE("LOW"), // LOW or HIGH frequency mode for frequency synthesis
      .DLL_FREQUENCY_MODE("LOW"), // LOW, HIGH, or HIGH_SER frequency mode for DLL
      .DUTY_CYCLE_CORRECTION("TRUE"), // Duty cycle correction, TRUE or FALSE
      .FACTORY_JF(16'hf0f0), // FACTORY JF value suggested to be set to 16'hf0f0
      .PHASE_SHIFT(0), // Amount of fixed phase shift from -255 to 1023
      .STARTUP_WAIT("FALSE") // Delay configuration DONE until DCM LOCK, TRUE/FALSE
   ) DCM_BASE_block (
      .CLK0(dcm_clk0),         // 0 degree DCM CLK output
      .CLK180(CLK180),     // 180 degree DCM CLK output
      .CLK270(CLK270),     // 270 degree DCM CLK output
      .CLK2X(CLK2X),       // 2X DCM CLK output
      .CLK2X180(CLK2X180), // 2X, 180 degree DCM CLK out
      .CLK90(CLK90),       // 90 degree DCM CLK output
      .CLKDV(clk),       // Divided DCM CLK out (CLKDV_DIVIDE)
      .CLKFX(CLKFX),       // DCM CLK synthesis out (M/D)
      .CLKFX180(CLKFX180), // 180 degree CLK synthesis out
      .LOCKED(LOCKED),     // DCM LOCK status output
      .CLKFB(dcm_clk0),       // DCM clock feedback
      .CLKIN(clkin),         // Clock input (from IBUFG, BUFG or DCM)
      .RST(1'b0)            // DCM asynchronous reset input
   );
 */


    p_mem   p_mem_block
    (
        .clk            (clk),
        .reset          (reset),
        
        .portw_addr     (p_mem_portw_addr),
        .portw_data     (p_mem_portw_data),
        .portw_en       (p_mem_portw_en),
        
        .portr1_addr    (p_addr),
        .portr1_ins0    (ins_0),
        .portr1_ins1    (ins_1),
        .portr1_ins2    (ins_2),
        .portr1_en      (1'b1),//(p_mem_r1_en),
        
        .portr2_addr    (p_addr_alt_reg),
        .portr2_data    (p_data_alt),
        .portr2_en      (1'b1)//(p_mem_r2_en)
    );
    
    loader  data_loader_block(
        .clk                (clk),
        .reset              (reset),
        
        .pc_in              (pc),
        .pc_out             (prev_addr_to_pc),
        .pc_offset_sel_in   (control_pc_offset),
        .branching_in       (control_branchz&cz_flag),
        
        .op1_2d_in          (op1_2d),
        .op1_1d_in          (op1_1d),
        .op2_2d_in          (op2_2d),
        .op2_1d_in          (op2_1d),
        
        .p_addr_out         (p_addr)
    );
    
    ins_buf ins_buf_block(
        .clk                (clk),
        .reset              (reset),
    
        .ins_2_in           (ins_2),
        .ins_1_in           (ins_1),
        .ins_0_in           (ins_0),
        
        .op_out             (ins_buf_to_op),
        .op1_out            (ins_buf_to_op1),
        .op2_out            (ins_buf_to_op2),
        
        .control_branchz_in (control_branchz),
        .cz_flag_in         (cz_flag),
        .shifter_in         (control_shifter),
        .p_mem_in           (control_pout)
    );
    
    
    decoder decoder_block
    (
        .clk                (clk),
        .rst                (reset),
     
        .op                 (ins_buf_to_op),
        .cw                 (cw_in),
        .error              ()
    );
    
    mem_file mem_file_block
    (
        .clk                 (clk),
        .reset               (reset),
        
        .op1_1d_in           (op1_1d),
        .op2_1d_in           (op2_1d),
        .op_1d_3LSB_in       (op_1d[2:0]),
        
        .mr_addr1_sel_in     (control_read_addr),
        .mr_addr1_bit_in     (control_readbit),
        .mem_write_en_in     (control_wen),
        .mem_write_bit_in    (control_wbit),
        
        .op1_2d_in           (op1_2d),
        .op2_2d_in           (op2_2d),
        .op_2d_3LSB_in       (op_2d[2:0]),
        
        .mem_write_sel_in    (control_write_sel),
        .mem_w_addr_sel_in   (control_waddr),
        .mw_data_in          (data_alu_to_mem_file),
        .pm_data_in          (p_data_alt),
        .psw_in              (psw_alu_to_mem_file),
        .psw_out             (psw_mem_file_to_alu),
        .sp_dec_in           (control_spdec),
        .r_data0_out         (mem_file_to_d0),
        .r_data1_out         (mem_file_to_d1),
        .dptr_out            (dptr),
        .acc_out             (acc),
        
        .port0_out           (port0_out),
        .port1_out           (port1_out),
        .port2_out           (port2_out),
        .port3_out           (port3_out),
        .port0_in            (port0_in),
        .port1_in            (port1_in),
        .port2_in            (port2_in),
        .port3_in            (port3_in),
        .rxdata_serial_in    (rxdata_serial),
        .serial_rx_done_in   (serial_rx_done),
        .serial_ren_out      (serial_ren),
        .serial_ri_flag_out  (serial_ri_flag),
        .txdata_serial_out   (txdata_serial),
        .serial_tx_done_in   (serial_tx_done),
        .serial_ti_flag_out  (serial_ti_flag),
        .serial_enable_out   (serial_enable)
        
    );
    
    adder16 adder16_block
    (
        .clk                (clk),
        
        .acc_in             (acc),
        .dptr_in            (dptr),
        .pc_in              (pc),
        
        .addr16_sel_in      (control_add16),
        
        .ans_out            (p_addr_alt),
        .ans_out_reg        (p_addr_alt_reg)
    );
    
    //for test synthesis
 //   assign data_alu_to_mem_file = d1_to_alu;
 //   assign cz_flag = 1'b1;
 
    ALU alu_block
    (
        .clk            (clk),
        
		.in0            (d0_to_alu), 
		.in1            (d1_to_alu),
		.ALU_bit        (op1_2d[2:0]),             //input
		.out            (data_alu_to_mem_file), 
		.PSW_in         (psw_mem_file_to_alu), 
		.PSW_out        (psw_alu_to_mem_file), 
		.branch         (control_alu_b), 
		.z              (cz_flag), 
		.op             (control_alu_op)
    );
    
    serial_rx serial_rx_block
    (
        .clk                        (clk),
        .reset                      (reset),
        .special_reset              (~((~serial_ri_flag)&serial_ren)),
        .enable                     (serial_enable),

        .rxdata_in                  (serial_rx_in),

        .rxdata_out                 (rxdata_serial),
        .rxdata_valid_out           (serial_rx_done),
        .rxdata_error_out           (),
        .rxdata_recieve_enable_in   ((~serial_ri_flag)&serial_ren),

        .rxdata_baud_clk_out        (serial_baud_clk),
        .rxdata_baud8_clk_out       ()
    );
    
    serial_general_tx serial_general_tx_block
    (
        .baud_clk                   (serial_baud_clk),
        //.reset                      (reset),
        .reset                      (~((~serial_ti_flag)&(~serial_ren))),

        .tx_data_in                 (txdata_serial),
        .transmit_in                ((~serial_ti_flag)&(~serial_ren)),
        .tx_done_out                (serial_tx_done),

        .tx_data_out                (serial_tx_out)
    );
    
    assign {control_pc_incr,control_shifter,control_pc_offset} 
            = cw_in[CW_WIDTH  - 1:
                    CW_WIDTH 
                    - CONTROL_PC_INCR_WIDTH 
                    - CONTROL_SHIFTER_WIDTH 
                    - CONTROL_PCOFFSET_WIDTH];
                    
    assign {control_pout,control_branchz} 
            = cw_in[(CONTROL_POUT_WIDTH + CONTROL_BRANCHZ_WIDTH + CONTROL_ADD16_WIDTH + CONTROL_RETI_WIDTH) - 1:
                                                                 (CONTROL_ADD16_WIDTH + CONTROL_RETI_WIDTH )    ];
                              
    assign {control_pclow,control_pchigh} = cw_in[CONTROL_PCLOWHIGH_WIDTH+ CONTROL_PCLOWHIGH_WIDTH+(CONTROL_POUT_WIDTH + CONTROL_BRANCHZ_WIDTH + CONTROL_ADD16_WIDTH + CONTROL_RETI_WIDTH )-1
                                                    :(CONTROL_POUT_WIDTH + CONTROL_BRANCHZ_WIDTH + CONTROL_ADD16_WIDTH + CONTROL_RETI_WIDTH )];
    assign {control_d0_l0,control_d1_l0,control_read_addr,control_readbit,control_rin}
            =
            cw[CW_WIDTH - 
                    - CONTROL_PC_INCR_WIDTH 
                    - CONTROL_SHIFTER_WIDTH 
                    - CONTROL_PCOFFSET_WIDTH - 1
               :(CONTROL_D0D1_L1_WIDTH+CONTROL_D0D1_L1_WIDTH+CONTROL_ALU_WIDTH+CONTROL_ALUB_WIDTH+CONTROL_WSEL_WIDTH+CONTROL_WADDR_WIDTH+CONTROL_WBIT_WIDTH+CONTROL_WEN_WIDTH+CONTROL_SPDEC_WIDTH+CONTROL_PCLOWHIGH_WIDTH+ CONTROL_PCLOWHIGH_WIDTH+CONTROL_POUT_WIDTH + CONTROL_BRANCHZ_WIDTH + CONTROL_ADD16_WIDTH + CONTROL_RETI_WIDTH )];
    
    assign {control_d0_l1,control_d1_l1,control_alu_op, control_alu_b,control_write_sel,control_waddr,control_wbit,control_wen,control_spdec} = 
                cw_1d[(CONTROL_D0D1_L1_WIDTH+CONTROL_D0D1_L1_WIDTH+CONTROL_ALU_WIDTH+CONTROL_ALUB_WIDTH+CONTROL_WSEL_WIDTH+CONTROL_WADDR_WIDTH+CONTROL_WBIT_WIDTH+CONTROL_WEN_WIDTH+CONTROL_SPDEC_WIDTH+CONTROL_PCLOWHIGH_WIDTH + CONTROL_PCLOWHIGH_WIDTH + CONTROL_POUT_WIDTH + CONTROL_BRANCHZ_WIDTH + CONTROL_ADD16_WIDTH + CONTROL_RETI_WIDTH -1)
                                                                                                :(CONTROL_PCLOWHIGH_WIDTH + CONTROL_PCLOWHIGH_WIDTH + CONTROL_POUT_WIDTH + CONTROL_BRANCHZ_WIDTH + CONTROL_ADD16_WIDTH + CONTROL_RETI_WIDTH )];
    assign {control_add16,control_reti} = cw[(CONTROL_ADD16_WIDTH + CONTROL_RETI_WIDTH ) - 1:0];
 //   assign {pc_high_data,pc_low_data} = pc;
    
//    assign pc_jmp = (control_branchz&cz_flag) ? prev_addr_to_pc : pc;
    
    always @(*) begin
        case(control_pc_incr)
                2'd0 : begin
                    pc_inced_data = pc;
                end
                2'd1 : begin
                    pc_inced_data = pc + 1'd1;
                end
                2'd2 : begin
                    pc_inced_data = pc + 2'd2;
                end
                2'd3 : begin
                    pc_inced_data = pc + 2'd3;
                end
        endcase
    end
    
//    always @(*) begin
//        case((control_branchz&cz_flag)) 
//            1'b0 : begin
//                pc_load_data = prev_addr_to_pc;
//            end
//            1'b1 : begin
//                pc_load_data = pc_inced_data;
//            end
//        endcase
//    end
    
    always @(posedge clk) begin
        pc_inced_data_d <= pc_inced_data;
    end
    
    always @(posedge clk) begin
        if(reset) begin
            pc <= 16'hFF;
        end
        else begin 
            cw  <= cw_in;
            cw_1d <= cw;
            
            op_1d   <= ins_buf_to_op;
            op_2d   <= op_1d;
            
            op1_1d  <= ins_buf_to_op1;
            op1_2d  <= op1_1d;
            
            
            op2_1d  <= ins_buf_to_op2; 
            op2_2d  <= op2_1d;  
 
            case(control_pclow)
                `PCL_P   : pc[P_ADDR_WIDTH/2 - 1:0] <= pc_inced_data[P_ADDR_WIDTH/2 - 1:0];
                `PCL_OP1 : pc[P_ADDR_WIDTH/2 - 1:0] <= ins_buf_to_op1;
                `PCL_OP2 : pc[P_ADDR_WIDTH/2 - 1:0] <= ins_buf_to_op2;
                `PCL_ALU : pc[P_ADDR_WIDTH/2 - 1:0] <= data_alu_to_mem_file;
                `PCL_A16 : pc[P_ADDR_WIDTH/2 - 1:0] <= p_addr_alt[P_ADDR_WIDTH/2 - 1:0];
                `PCL_PCD : begin
                    if(control_branchz&cz_flag) begin
                        pc[P_ADDR_WIDTH/2 - 1:0] <= prev_addr_to_pc[P_ADDR_WIDTH/2 - 1:0]; 
                    end
                    else begin
                        pc[P_ADDR_WIDTH/2 - 1:0] <= pc_inced_data[P_ADDR_WIDTH/2 - 1:0];
                    end
                end                        //pc[P_ADDR_WIDTH/2 - 1:0] <= prev_addr_to_pc[P_ADDR_WIDTH/2 - 1:0];
            endcase
            
            case(control_pchigh)
                `PCH_P   : pc[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2] <= pc_inced_data[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2];
                `PCH_OP1 : pc[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2] <= ins_buf_to_op1;
                `PCH_ALU : pc[P_ADDR_WIDTH/2 - 1:0] <= data_alu_to_mem_file;
                `PCH_A16 : pc[P_ADDR_WIDTH/2 - 1:0] <= p_addr_alt[P_ADDR_WIDTH/2 - 1:0];
                `PCH_PCD : begin
                    if(control_branchz&cz_flag) begin
                        pc[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2] <= prev_addr_to_pc[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2];
                    end
                    else begin
                        pc[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2] <= pc_inced_data[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2];
                    end
                end
                
                       //pc[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2] <= prev_addr_to_pc[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2];
            endcase
            
            case(control_d0_l0)
                `D0L0_P     : d0 <= pc_inced_data_d[P_ADDR_WIDTH/2 - 1:0];
                `D0L0_OP1   : d0 <= op1_1d;
                `D0L0_OP2   : d0 <= op2_1d;
            endcase
            
            case(control_d1_l0)
                `D1L0_P     : d1 <= pc_inced_data_d[P_ADDR_WIDTH - 1:P_ADDR_WIDTH/2];
                //`D1L0_D1    : d1 <= d1;
                `D1L0_OP1   : d1 <= op1_1d;
                `D1L0_OP2   : d1 <= op2_1d;
            endcase
           
        end
    end
    
    always @(*) begin      
        case(control_d0_l1)
            `D0L1_D : d0_to_alu = d0;
            `D0L1_M : d0_to_alu = mem_file_to_d0;
        endcase
        
        case(control_d1_l1)
            `D1L1_D : d1_to_alu = d1;
            `D1L1_M : d1_to_alu = mem_file_to_d1;
        endcase
    end
    
    //test sythesisi
    assign alu_out = data_alu_to_mem_file;


endmodule
