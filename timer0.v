`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:42:29 11/03/2013 
// Design Name: 
// Module Name:    timer0 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module timer0(
		clk,
		reset,
		TMOD_in,
		TCON_strt_in,
		TMR0_overflow,
		pin_T0,
		pin_INT0,
		TMR0_in,
		TMR0_H_wen,
		TMR0_L_wen,
		TMR0_out
    );

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    /* STYLE_NOTES
     * Eacho I/O signal must be defined on a seperate line
     * All I/Os other than the clk, reset and standard interfaces (ex. local-link, axi4) should contain
     * an '_in' or '_out' suffix
     * Comment your I/O by block and by signal
     */
    input                              clk;
    input                              reset;	
    input	[2:0]						 	TMOD_in;
	input									TCON_strt_in;	
	output	reg								TMR0_overflow;
	 
	 input										pin_T0;	// external clock source for timer0
	 input										pin_INT0; // gate pin
	 input 		[7:0]							TMR0_in;
	 input										TMR0_H_wen;
	 input										TMR0_L_wen;
	 output	reg	[15:0]					TMR0_out;
	 
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
		reg										clock_timer0;
		reg		[15:0]						TMR0_out_inc;

//---------------------------------------------------------------------------------------------------------------------
// Implmentation
//---------------------------------------------------------------------------------------------------------------------


	
	// timer register update - external or internal
	always@(posedge clock_timer0) begin : timer_H_update
        if(reset) begin
            TMR0_out[15:8] <= 0;
        end
        else begin
            if(TMR0_H_wen) begin
                TMR0_out[15:8] <= TMR0_in;
            end
            else begin
                TMR0_out[15:8] <= TMR0_out_inc[15:8];  // write incremented value
            end
        end
	end
	always@(posedge clock_timer0) begin : timer_L_update
        if(reset) begin
            TMR0_out[7:0] <= 0;
        end
        else begin
            if(TMR0_L_wen) begin
                TMR0_out[7:0] <= TMR0_in;
            end
            else begin
                TMR0_out[7:0] <= TMR0_out_inc[7:0];  // write incremented value
            end        
        end

	end
	
	// clock source selection for timer
	always@(*) begin : clock_souce_select
		if(TMOD_in[2]==1) begin
			clock_timer0 = !pin_T0;
		end
		else begin
			clock_timer0 = clk;
		end
	end
	
	// timer0 running operation
	always@(*) begin
		if(TCON_strt_in) begin		// if timer running is enabled
			case({TMOD_in[1:0]}) 
				2'b10 : begin			// 8 bit auto reload mode
					if(TMR0_out[7:0]==8'hFF) begin 
						TMR0_out_inc = {TMR0_out[15:8],TMR0_out[15:8]};
						//TMR0_overflow <= 1'b1;			// set overflow flag , TCON_out[5] <= 1'b1;
					end	
					else begin
						TMR0_out_inc = TMR0_out + 1'b1;
					end
				end
				2'b00 : begin		// 13 bit mode
					if(TMR0_out==16'hFF1F) begin
						TMR0_out_inc = 16'h0;
						//TMR0_overflow <= 1'b1;			// set overflow flag
					end
					else if({TMR0_out[7:0]}==8'h1F) begin
						TMR0_out_inc = {TMR0_out[15:8],8'b0} + 16'h0100;
					end
					else begin
						TMR0_out_inc = TMR0_out + 1'b1;
					end
				end
				default : begin		// for 16 bit mode
					if(TMR0_out==16'hFFFF) begin
						TMR0_out_inc = 16'h0;
						//TMR0_overflow <= 1'b1;			// set overflow flag
					end
					else begin
						TMR0_out_inc = TMR0_out + 1'b1;
					end
				end
			endcase
		end
        else begin
            TMR0_out_inc = TMR0_out;
        end
	end
	
	always@(*) begin
		if(TMOD_in[1:0]==2'b10 && {TMR0_out[7:0]}==8'hFF) begin
			TMR0_overflow = 1'b1;
		end
		else if(TMOD_in[1:0]==2'b00 && TMR0_out==16'hFF1F) begin
			TMR0_overflow = 1'b1;
		end
		else if(TMR0_out==16'hFFFF) begin
			TMR0_overflow = 1'b1;
		end
		else begin
			TMR0_overflow = 1'b0;
		end
	end

endmodule
