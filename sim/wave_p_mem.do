onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_tb/uut/clk
add wave -noupdate /top_tb/uut/reset
add wave -noupdate /top_tb/uut/pc
add wave -noupdate /top_tb/uut/data_loader_block/p_addr_out
add wave -noupdate /top_tb/uut/data_loader_block/pc_offset_int
add wave -noupdate /top_tb/uut/decoder_block/in_op
add wave -noupdate /top_tb/uut/ins_buf_block/ins_2_in
add wave -noupdate /top_tb/uut/ins_buf_block/ins_1_in
add wave -noupdate /top_tb/uut/ins_buf_block/ins_0_in
add wave -noupdate /top_tb/uut/ins_buf_block/ir6
add wave -noupdate /top_tb/uut/ins_buf_block/ir5
add wave -noupdate /top_tb/uut/ins_buf_block/ir4
add wave -noupdate /top_tb/uut/ins_buf_block/op2
add wave -noupdate /top_tb/uut/ins_buf_block/op1
add wave -noupdate /top_tb/uut/ins_buf_block/op
add wave -noupdate -divider {New Divider}
add wave -noupdate /top_tb/uut/p_mem_block/clk
add wave -noupdate /top_tb/uut/p_mem_block/p_mem_bank0_r1data
add wave -noupdate /top_tb/uut/p_mem_block/p_mem_bank1_r1data
add wave -noupdate /top_tb/uut/p_mem_block/portr1_addr
add wave -noupdate /top_tb/uut/p_mem_block/portr1_en
add wave -noupdate /top_tb/uut/p_mem_block/portr1_ins0
add wave -noupdate /top_tb/uut/p_mem_block/portr1_ins1
add wave -noupdate /top_tb/uut/p_mem_block/portr1_ins2
add wave -noupdate /top_tb/uut/p_mem_block/portr2_addr
add wave -noupdate /top_tb/uut/p_mem_block/portr2_data
add wave -noupdate /top_tb/uut/p_mem_block/portr2_en
add wave -noupdate /top_tb/uut/p_mem_block/portw_addr
add wave -noupdate /top_tb/uut/p_mem_block/portw_data
add wave -noupdate /top_tb/uut/p_mem_block/portw_en
add wave -noupdate /top_tb/uut/p_mem_block/reset
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2539895 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2508611 ps} {2606065 ps}
