onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix unsigned /top_tb/uut/p_mem_block/clk
add wave -noupdate -radix unsigned /top_tb/uut/p_mem_block/reset
add wave -noupdate -radix unsigned /top_tb/uut/p_mem_block/portw_addr
add wave -noupdate -radix unsigned /top_tb/uut/p_mem_block/portw_data
add wave -noupdate -radix unsigned /top_tb/uut/p_mem_block/portw_en
add wave -noupdate -label in_op -radix unsigned /top_tb/uut/decoder_block/in_op
add wave -noupdate -label state -radix unsigned /top_tb/uut/decoder_block/state
add wave -noupdate {/top_tb/uut/decoder_block/next_state[10]}
add wave -noupdate {/top_tb/uut/decoder_block/next_state[9]}
add wave -noupdate {/top_tb/uut/decoder_block/next_state[8]}
add wave -noupdate /top_tb/uut/decoder_block/op
add wave -noupdate /top_tb/uut/p_mem_block/portr1_addr
add wave -noupdate /top_tb/uut/p_mem_block/portr1_ins0
add wave -noupdate /top_tb/uut/p_mem_block/portr1_ins1
add wave -noupdate /top_tb/uut/p_mem_block/portr1_ins2
add wave -noupdate /top_tb/uut/control_pclow
add wave -noupdate /top_tb/uut/pc
add log -r /*
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2611073 ps} 0}
quietly wave cursor active 1
update
WaveRestoreZoom {2509345 ps} {2984812 ps}
run -all
exit
