onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_board_test_2/serial_rx_in
add wave -noupdate /top_board_test_2/serial_tx_out
add wave -noupdate /top_board_test_2/uut/ins_buf_block/ins_2_in
add wave -noupdate /top_board_test_2/uut/ins_buf_block/ins_1_in
add wave -noupdate /top_board_test_2/uut/ins_buf_block/ins_0_in
add wave -noupdate /top_board_test_2/uut/ins_buf_block/op2
add wave -noupdate /top_board_test_2/uut/ins_buf_block/op1
add wave -noupdate /top_board_test_2/uut/ins_buf_block/op
add wave -noupdate /top_board_test_2/clk
add wave -noupdate -radix unsigned /top_board_test_2/uut/data_loader_block/pc_offset_int
add wave -noupdate -radix unsigned /top_board_test_2/uut/data_loader_block/p_addr_out
add wave -noupdate -radix unsigned /top_board_test_2/uut/pc
add wave -noupdate /top_board_test_2/uut/decoder_block/in_op
add wave -noupdate /top_board_test_2/uut/control_branchz
add wave -noupdate /top_board_test_2/uut/cz_flag
add wave -noupdate /top_board_test_2/uut/alu_block/mask0_1
add wave -noupdate /top_board_test_2/uut/alu_block/mask1_0
add wave -noupdate /top_board_test_2/uut/alu_block/out
add wave -noupdate /top_board_test_2/uut/alu_block/in0
add wave -noupdate /top_board_test_2/uut/alu_block/in1
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_153
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_0
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_1
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_2
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_3
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_4
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_5
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_6
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_7
add wave -noupdate -radix ascii /top_board_test_2/uut/mem_file_block/now_mem_224
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {349374540 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {349325715 ps} {349464343 ps}
