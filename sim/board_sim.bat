if exist work (
	rmdir /S /Q work 2> nul
)

set SV_file=eightzerofiveone_dpi_iface.svi 

echo vlib work
vlib work

vlog -L work -sv -dpiheader dpiheader.h %SV_file%
vlog -L work ../*.v

vlog -L work top_board_test.v

vsim -c -novopt -t 1ps +notimingchecks +TESTNAME=tx_test -sv_lib dpi_8051 -lib work work.top_board_test -do wave2.do

echo Done !