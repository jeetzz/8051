`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:46:26 10/24/2013
// Design Name:   eightzerofiveone_top
// Module Name:   D:/090250V/SEMESTER 7/ADSL/8051/HDL/rtl/sim/top_tb.v
// Project Name:  design8051
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: eightzerofiveone_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module top_board_test;

	// Inputs
	reg clk;
	reg reset;
	reg [15:0] p_mem_portw_addr;
	reg [7:0] p_mem_portw_data;
	reg p_mem_portw_en;
	reg [7:0] port0_in;
	reg [7:0] port1_in;
	reg [7:0] port2_in;
	reg [7:0] port3_in;
    
    localparam  P_ADDR_WIDTH            = 16;
    
    reg     [P_ADDR_WIDTH - 1:0]        pc_1d;
    reg     [P_ADDR_WIDTH - 1:0]        pc_2d;
    reg                                 ins_step_cond_1d;
    reg                                 ins_step_cond_2d;

	// Outputs
	wire [7:0] alu_out;
	wire [7:0] port0_out;
	wire [7:0] port1_out;
	wire [7:0] port2_out;
	wire [7:0] port3_out;
    
    wire     serial_tx_out;
    reg     serial_rx_in;
    
    integer prog_file_in,r_file;
    integer file_out;
    reg [7:0]   prev_acc;

    eightzerofiveone_dpi_iface eightzerofiveone_dpi_iface_block();
	// Instantiate the Unit Under Test (UUT)
	eightzerofiveone_top uut (
		.clk(clk), 
		.reset(reset), 
		.p_mem_portw_addr(p_mem_portw_addr), 
		.p_mem_portw_data(p_mem_portw_data), 
		.p_mem_portw_en(p_mem_portw_en), 
		.alu_out(alu_out), 
		.port0_in(port0_in), 
		.port1_in(port1_in), 
		.port2_in(port2_in), 
		.port3_in(port3_in), 
		.port0_out(port0_out), 
		.port1_out(port1_out), 
		.port2_out(port2_out), 
		.port3_out(port3_out),
        .serial_rx_in(serial_rx_in),
        .serial_tx_out(serial_tx_out)
	);


	initial begin
		// Initialize Inputs
     
        eightzerofiveone_dpi_iface_block.mem_reset();
        eightzerofiveone_dpi_iface_block.update_memory();
        
		clk = 0;
		p_mem_portw_addr = 0;
		p_mem_portw_data = 0;
		p_mem_portw_en = 0;
		port0_in = 0;
		port1_in = 0;
		port2_in = 0;
		port3_in = 0;

		// Wait 100 ns for global reset to finish
        load_prog_memory();
        reset = 1;
        #100;
        @(posedge clk)
        reset <= 0;
            #1000;
            @(posedge clk);
            send_char("A");
        #20000;
        $finish;
		// Add stimulus here

	end
    
    always #10 clk = ~clk;

task load_prog_memory;
    integer i;
    integer idx;
    reg [7:0] read_byte;
	begin
        idx =0;
        p_mem_portw_en = 1;
        for ( i = 0 ; i < eightzerofiveone_dpi_iface_block.get_progam_length() ; i = i + 1 ) begin
                read_byte = eightzerofiveone_dpi_iface_block.read_prog_data();
                //$display("pg mem read idx =%d val =%d",idx ,read_byte);
                @(posedge clk) begin
                    p_mem_portw_addr = idx;
                    p_mem_portw_data = read_byte;
                end
                idx = idx + 1;       
        end
        @(posedge clk)
        p_mem_portw_en = 0;
	end
endtask

task send_char;
    input [7:0] char;
    begin : task_bla
        integer i;
        @(posedge uut.serial_baud_clk);
        serial_rx_in <= 1'b0;
        for ( i = 0 ; i < 8 ; i = i + 1) begin
            @(posedge uut.serial_baud_clk);
            serial_rx_in <= char[i];
        end
        @(posedge uut.serial_baud_clk);
        serial_rx_in <= 1'b1;
    end
endtask

endmodule    