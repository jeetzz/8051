
set C_file=*.cpp
set C_obj_file=*.obj
set SV_file=eightzerofiveone_dpi_iface.svi 
set INSTALL_HOME=C:\questa_sim64_10.1a
set PLATFORM_INSTALL_HOME=C:\questa_sim64_10.1a\win64

if exist work (
	rmdir /S /Q work 2> nul
)

rem call "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" x86_amd64

echo vlib work
vlib work

vlog -L work -sv -dpiheader dpiheader.h %SV_file%
vlog -L work *.v
vlog -L work %XILINX_VERILOG%\glbl.v

rem echo cl -c -Ox -Oy -I %INSTALL_HOME%\include %C_file%
rem cl -c -Ox -Oy -I %INSTALL_HOME%\include %C_file%
rem echo link /DLL %C_obj_file% %PLATFORM_INSTALL_HOME%\mtipli.lib
rem link %1 /DLL %C_obj_file% %PLATFORM_INSTALL_HOME%\mtipli.lib

vsim -c -novopt -t 1ps +notimingchecks +TESTNAME=tx_test -sv_lib dpi_8051 -lib work work.eightzerofiveone_top_sim work.glbl -do sim.do

echo Done !

