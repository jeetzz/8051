`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:07:58 10/24/2013
// Design Name:   eightzerofiveone_top
// Module Name:   J:/Campus Electronics/ADSL/eightzerofiveone/sim/eightzerofiveone_top_sim.v
// Project Name:  eightzerofiveone
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: eightzerofiveone_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module eightzerofiveone_top_sim;

	// Inputs
	reg clk;
	reg reset;
	reg [15:0] p_mem_portw_addr;
	reg [7:0] p_mem_portw_data;
	reg p_mem_portw_en;
	reg [7:0] port0_in;
	reg [7:0] port1_in;
	reg [7:0] port2_in;
	reg [7:0] port3_in;

	// Outputs
	wire [7:0] alu_out;
	wire [7:0] port0_out;
	wire [7:0] port1_out;
	wire [7:0] port2_out;
	wire [7:0] port3_out;
    
    integer i;
    
    eightzerofiveone_dpi_iface eightzerofiveone_dpi_iface_block();

	// Instantiate the Unit Under Test (UUT)
//	eightzerofiveone_top uut (
//		.clk(clk), 
//		.reset(reset), 
//		.p_mem_portw_addr(p_mem_portw_addr), 
//		.p_mem_portw_data(p_mem_portw_data), 
//		.p_mem_portw_en(p_mem_portw_en), 
//		.alu_out(alu_out), 
//		.port0_in(port0_in), 
//		.port1_in(port1_in), 
//		.port2_in(port2_in), 
//		.port3_in(port3_in), 
//		.port0_out(port0_out), 
//		.port1_out(port1_out), 
//		.port2_out(port2_out), 
//		.port3_out(port3_out)
//	);

	initial begin
		// Initialize Inputs
		// clk = 0;
		   reset = 0;
		// p_mem_portw_addr = 0;
		// p_mem_portw_data = 0;
		   p_mem_portw_en = 0;
		// port0_in = 0;
		// port1_in = 0;
		// port2_in = 0;
		// port3_in = 0;
        eightzerofiveone_dpi_iface_block.mem_reset();
        eightzerofiveone_dpi_iface_block.update_memory();
        $display("PC : %d", eightzerofiveone_dpi_iface_block.get_pc()); 
        
        @(posedge clk);
        @(posedge clk);
            p_mem_portw_data <= eightzerofiveone_dpi_iface_block.get_data(8'd129);
            $display("%d",p_mem_portw_data);
            
            $display("prog memory : ");
            for ( i = 0 ; i < eightzerofiveone_dpi_iface_block.get_progam_length() ; i = i + 1 ) begin
                 $display("%d",eightzerofiveone_dpi_iface_block.read_prog_data());
            end
            $display("end prog memory");
            
        
            forever begin
                @(posedge clk);
                    if(eightzerofiveone_dpi_iface_block.has_more_to_execute()) begin
                        eightzerofiveone_dpi_iface_block.ins_step();
                        $display("PC : %d", eightzerofiveone_dpi_iface_block.get_pc());
                    end
                    else begin
                        $display("dollar finish ewrai!!");
                        $finish;
                    end
            end
            
            
            
        @(posedge clk); 
        @(posedge clk);     

        $finish;
        
        
        

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
    
    
    initial begin
        clk = 0;
        forever begin
            #5 clk <= ~clk;
        end
    end
      
endmodule

