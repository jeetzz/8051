if exist work (
	rmdir /S /Q work 2> nul
)

echo vlib work
vlib work

vlog -L work ../*.v

vlog -L work top_board_test_2.v

vsim -c -novopt -t 1ps +notimingchecks +TESTNAME=tx_test -lib work work.top_board_test_2 -do wave2.do

echo Done !