onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_tb/uut/ins_buf_block/temp_ir1
add wave -noupdate /top_tb/uut/ins_buf_block/temp_ir2
add wave -noupdate /top_tb/uut/ins_buf_block/temp_ir3
add wave -noupdate /top_tb/uut/ins_buf_block/temp_ir4
add wave -noupdate /top_tb/uut/ins_buf_block/temp_ir5
add wave -noupdate /top_tb/uut/ins_buf_block/temp_ir6
add wave -noupdate -expand -group {New Group} /top_tb/uut/ins_buf_block/ins_2_in
add wave -noupdate -expand -group {New Group} /top_tb/uut/ins_buf_block/ins_1_in
add wave -noupdate -expand -group {New Group} /top_tb/uut/ins_buf_block/ins_0_in
add wave -noupdate -expand -group {New Group} /top_tb/uut/ins_buf_block/op2
add wave -noupdate -expand -group {New Group} /top_tb/uut/ins_buf_block/op1
add wave -noupdate -expand -group {New Group} /top_tb/uut/ins_buf_block/op
add wave -noupdate -divider {New Divider}
add wave -noupdate -radix decimal /top_tb/uut/data_loader_block/p_addr_out
add wave -noupdate -radix decimal /top_tb/uut/pc
add wave -noupdate /top_tb/uut/clk
add wave -noupdate /top_tb/uut/reset
add wave -noupdate /top_tb/uut/control_pchigh
add wave -noupdate /top_tb/uut/control_pclow
add wave -noupdate -radix decimal /top_tb/uut/prev_addr_to_pc
add wave -noupdate /top_tb/uut/data_loader_block/pc_offset_int
add wave -noupdate /top_tb/uut/decoder_block/in_op
add wave -noupdate /top_tb/uut/ins_buf_block/shifter_in
add wave -noupdate /top_tb/uut/mem_file_block/next_mem_224
add wave -noupdate /top_tb/uut/mem_file_block/now_mem_224
add wave -noupdate /top_tb/uut/alu_block/in0
add wave -noupdate /top_tb/uut/alu_block/in1
add wave -noupdate /top_tb/uut/alu_block/out
add wave -noupdate /top_tb/uut/ins_buf_block/p_mem_in
add wave -noupdate /top_tb/uut/ins_buf_block/control_branchz_in
add wave -noupdate /top_tb/uut/ins_buf_block/cz_flag_in
add wave -noupdate /top_tb/uut/pc_inced_data
add wave -noupdate /top_tb/uut/control_pc_incr
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {71760829 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {71661551 ps} {71808859 ps}
