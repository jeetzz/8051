`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:46:26 10/24/2013
// Design Name:   eightzerofiveone_top
// Module Name:   D:/090250V/SEMESTER 7/ADSL/8051/HDL/rtl/sim/top_tb.v
// Project Name:  design8051
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: eightzerofiveone_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module top_board_test_2;

	// Inputs
	reg clk;
	reg reset;
	reg [7:0] port0_in;
	reg [7:0] port1_in;
	reg [7:0] port2_in;
	reg [7:0] port3_in;
    
    localparam  P_ADDR_WIDTH            = 16;
    
    reg     [P_ADDR_WIDTH - 1:0]        pc_1d;
    reg     [P_ADDR_WIDTH - 1:0]        pc_2d;
    reg                                 ins_step_cond_1d;
    reg                                 ins_step_cond_2d;

	// Outputs
	wire [7:0] alu_out;
	wire [7:0] port0_out;
	wire [7:0] port1_out;
	wire [7:0] port2_out;
	wire [7:0] port3_out;
    
    wire     serial_tx_out;
    reg     serial_rx_in;
    

	// Instantiate the Unit Under Test (UUT)
	eightzerofiveone_top uut (
		.clkin(clk), 
		.resetn(~reset), 
		.alu_out(alu_out), 
		.port0_in(port0_in), 
		.port1_in(port1_in), 
		.port2_in(port2_in), 
		.port3_in(port3_in), 
		.port0_out(port0_out), 
		.port1_out(port1_out), 
		.port2_out(port2_out), 
		.port3_out(port3_out),
        .serial_rx_in(serial_rx_in),
        .serial_tx_out(serial_tx_out)
	);


	initial begin
		// Initialize Inputs
     
		clk = 0;
        reset = 0;
		port0_in = 0;
		port1_in = 0;
		port2_in = 0;
		port3_in = 0;

		// Wait 100 ns for global reset to finish
        @(posedge clk)
        wait(uut.p_mem_portw_en != 1);
        reset = 1;
        #100;
        @(posedge clk)
        reset <= 0;
        $display("reset done");
            #1000;
            @(posedge clk);
            send_char("H");
            #1000;
            send_char("G");
            #1000;
            send_char("F");
            #1000;
            send_char("E");
            #1000;
            send_char("D");
            #1000;
            send_char("C");
            #1000;
            send_char("B");
            #1000;
            send_char("A");            
            
        #6000000;
        $finish;
		// Add stimulus here

	end
    
    always #10 clk = ~clk;


task send_char;
    input [7:0] char;
    begin : task_bla
        integer i;
        @(posedge uut.serial_baud_clk);
        serial_rx_in <= 1'b0;
        for ( i = 0 ; i < 8 ; i = i + 1) begin
            @(posedge uut.serial_baud_clk);
            serial_rx_in <= char[i];
        end
        @(posedge uut.serial_baud_clk);
        serial_rx_in <= 1'b1;
    end
endtask

endmodule    