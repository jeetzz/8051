if exist work (
	rmdir /S /Q work 2> nul
)

set SV_file=eightzerofiveone_dpi_iface.svi 

echo vlib work
vlib work

vlog -L work -sv -dpiheader dpiheader.h %SV_file%
vlog -L work ../*.v

vlog -L work top_tb.v

vsim -c -novopt -t 1ps +notimingchecks +TESTNAME=tx_test -sv_lib dpi_8051 -lib work work.top_tb -do wave1.do

echo Done !