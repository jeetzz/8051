module serial_general_tx 
    (
        baud_clk,
        reset,
        
        tx_data_in,
        transmit_in,
        tx_done_out,
		
		tx_data_out
    );



//------------------------------------------------
// localparams
//------------------------------------------------   
    localparam              START_BIT   = 1'b0;
    localparam              STOP_BIT    = 1'b1;
    localparam              STATE_INIT  = 0;
    localparam              STATE_TX    = 1;
    localparam              BYTE_WIDTH  = 8;
    
//------------------------------------------------
// I/O Signals
//------------------------------------------------   
    input                       reset;
    input                       baud_clk;
    
    input   [BYTE_WIDTH - 1:0]  tx_data_in;
    input                       transmit_in;
    output reg                  tx_done_out;
	output reg      			tx_data_out;
    
//------------------------------------------------
// Internal regs and wires
//------------------------------------------------
    integer                     state;
    reg     [3:0]               counter;
    reg     [BYTE_WIDTH - 1:0]  tx_data_reg;
    
//-------------------------------------------------
// Implementation
//-------------------------------------------------     
    
    always @(posedge baud_clk or posedge reset) begin
        if(reset) begin
            counter <= 4'd0;
            state <= STATE_INIT;
            tx_done_out <= 1'b0;
        end
        else begin
            case(state)
                STATE_INIT : begin
                    if(transmit_in) begin
                        counter <= 4'd1;
                        tx_data_reg <= tx_data_in;
                        state <= STATE_TX;
                        tx_done_out <= 1'b0;
                    end
                end
                STATE_TX : begin
                    if(counter == 4'd10 || counter == 4'd0) begin
                        tx_done_out     <= 1'b1;
                        counter         <= 4'd0;
                        state           <= STATE_INIT;
                    end
                    else begin
                        if(counter != 4'd1) begin
                            tx_data_reg <= tx_data_reg >> 1;
                        end
                        counter <= counter + 4'd1;
                    end
                end
            endcase
        end
    end
    
    
    always @(*) begin
        if (counter == 1) begin
            tx_data_out = START_BIT;
        end
        else if (counter == 10 || counter == 0) begin
            tx_data_out = STOP_BIT;
        end
        else begin
            tx_data_out = tx_data_reg[0];
        end
    end
    
endmodule