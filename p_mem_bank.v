`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:48:02 10/14/2013 
// Design Name: 
// Module Name:    p_mem_bank 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module p_mem_bank
    (
        clk,
        reset,
        
        portw_addr,
        portw_data,
        portw_en,
        
        portr1_addr,
        portr1_data,
        portr1_en,
        
        portr2_addr,
        portr2_data,
        portr2_en
    );
    
//-------------------------------------------------------
// LOCALPARAMS
//-------------------------------------------------------
    
    localparam  LOG2NUM_OF_BANKS        = 2;
    localparam  INSTRUCTION_WIDTH       = 8;
    localparam  P_ADDR_WIDTH            = 16;
    localparam  MEM_SIZE                = 1 << (P_ADDR_WIDTH-LOG2NUM_OF_BANKS);
    
//-------------------------------------------------------
// I/O Signals
//--------------------------------------------------------

    input                                                   clk;
    input                                                   reset;
    
    input       [P_ADDR_WIDTH - LOG2NUM_OF_BANKS -1:0]      portw_addr;
    input       [INSTRUCTION_WIDTH-1:0]                     portw_data;
    input                                                   portw_en;
        
    input       [P_ADDR_WIDTH - LOG2NUM_OF_BANKS -1:0]      portr1_addr;
    output reg  [INSTRUCTION_WIDTH-1:0]                     portr1_data;
    input                                                   portr1_en;
        
    input       [P_ADDR_WIDTH - LOG2NUM_OF_BANKS -1:0]      portr2_addr;
    output reg  [INSTRUCTION_WIDTH-1:0]                     portr2_data;
    input                                                   portr2_en;
    
//--------------------------------------------------------
// Internal Regs and wires
//--------------------------------------------------------

    reg [INSTRUCTION_WIDTH-1:0]         mem [MEM_SIZE-1:0];
    
//--------------------------------------------------------
// Implementation
//--------------------------------------------------------

    always @(posedge clk) begin :write_port
        if (reset) begin

        end
        else begin
            if(portw_en) begin
                mem[portw_addr] <= portw_data;
            end
        end
    end
    
    always @(posedge clk) begin :read_port1
        if (reset) begin

        end
        else begin
            if(portr1_en) begin
                portr1_data <= mem[portr1_addr];
            end
        end
    end
    
    //assign portr1_data = mem[portr1_addr];
    
    always @(posedge clk) begin :read_port2
        if (reset) begin

        end
        else begin
            if(portr2_en) begin
                portr2_data <= mem[portr2_addr];
            end
        end
    end


endmodule
